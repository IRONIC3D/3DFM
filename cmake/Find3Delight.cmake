# Locate 3Delight root directory.
find_path(3Delight_ROOT_DIR
	NAMES bin/renderdl bin/renderdl.exe
	HINTS ENV DELIGHT
	NO_DEFAULT_PATH
	REQUIRED)

# Locate 3Delight include directory.
find_path(3Delight_INCLUDE_DIR
	NAMES delight.h
	HINTS ${3Delight_ROOT_DIR}/include
		${3Delight_ROOT_DIR}/../include
	NO_DEFAULT_PATH
	REQUIRED)

# Try to find ri.h for legacy features.
find_path(3Delight_RI_H
	NAMES ri.h
	HINTS
		${3Delight_INCLUDE_DIR}
		${3Delight_LEGACY_INCLUDE_DIR}
		${3Delight_ROOT_DIR}/../legacy_include
	NO_DEFAULT_PATH)

# Locate 3Delight library.
find_library(3Delight_LIBRARY
	NAMES 3delight
	HINTS ${3Delight_ROOT_DIR}/lib
	NO_DEFAULT_PATH
	REQUIRED)

# Create target.
find_package_handle_standard_args("3Delight"
	REQUIRED_VARS 3Delight_INCLUDE_DIR 3Delight_LIBRARY)

if(3Delight_FOUND AND NOT TARGET 3Delight::3Delight)
	add_library(3Delight::3Delight INTERFACE IMPORTED)
	target_include_directories(3Delight::3Delight
		INTERFACE "${3Delight_INCLUDE_DIR}")
	target_link_libraries(3Delight::3Delight
		INTERFACE "${3Delight_LIBRARY}")

	if(3Delight_RI_H STREQUAL "3Delight_RI_H-NOTFOUND")
		target_compile_definitions(3Delight::3Delight
			INTERFACE SKIP_LEGACY_FEATURES)
		# When this is set, not finding ri.h becomes a fatal error.
		if(DEFINED 3Delight_LEGACY_INCLUDE_DIR)
			message(FATAL_ERROR
				"3Delight_LEGACY_INCLUDE_DIR is set to "
				"${3Delight_LEGACY_INCLUDE_DIR} but ri.h was not found.")
		endif()
	else()
		target_include_directories(3Delight::3Delight
			INTERFACE ${3Delight_RI_H})
	endif()
endif()
