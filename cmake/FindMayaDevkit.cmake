set(VERSION ${MayaDevkit_FIND_VERSION})

# Locate the Maya devkit directory.
find_path(Maya${VERSION}Devkit_ROOT_DIR
	NAMES include/maya/MTypes.h
	HINTS $ENV{HOME_3DELIGHT}/building/mayalibs/$ENV{PLAT}/maya${VERSION}
		/Applications/Autodesk/maya${VERSION}
		/usr/autodesk/maya${VERSION}
		"C:/Program Files/Autodesk/Maya${VERSION}"
	NO_DEFAULT_PATH
	REQUIRED)

# Find Maya include directory.
find_path(Maya${VERSION}Devkit_INCLUDE_DIR
	NAMES maya/MTypes.h
	HINTS ${Maya${VERSION}Devkit_ROOT_DIR}/include
	NO_DEFAULT_PATH
	REQUIRED)
set(Maya${VERSION}Devkit_INCLUDE_DIRS "${Maya${VERSION}Devkit_INCLUDE_DIR}")

# Find all needed Maya libraries.
set(Maya${VERSION}Devkit_LIBRARIES "")
set(LIBRARY_NAMES
	Foundation
	Image
	OpenMaya
	OpenMayaAnim
	OpenMayaUI
	OpenMayaFX
	OpenMayaRender)
foreach(LIBRARY_NAME IN LISTS LIBRARY_NAMES)
	find_library(Maya${VERSION}Devkit_${LIBRARY_NAME}_LIBRARY
		NAMES ${LIBRARY_NAME}
		HINTS ${Maya${VERSION}Devkit_ROOT_DIR}/lib
		NO_DEFAULT_PATH
		REQUIRED)
	list(APPEND Maya${VERSION}Devkit_LIBRARIES
		"${Maya${VERSION}Devkit_${LIBRARY_NAME}_LIBRARY}")
endforeach()

# Provide target.
find_package_handle_standard_args("MayaDevkit"
	REQUIRED_VARS Maya${VERSION}Devkit_INCLUDE_DIR)

if(MayaDevkit_FOUND AND NOT TARGET Maya${VERSION}::Devkit)
	add_library(Maya${VERSION}::Devkit INTERFACE IMPORTED)
	set_target_properties(Maya${VERSION}::Devkit PROPERTIES
		INTERFACE_LINK_LIBRARIES "${Maya${VERSION}Devkit_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${Maya${VERSION}Devkit_INCLUDE_DIRS}")
endif()

# Declare function to make plugin for specific Maya version.
function(add_maya_plugin NAME VERSION SOURCE_FILES LINK_TARGETS)
	set(TARGET ${NAME}_${VERSION})

	add_library(${TARGET} MODULE ${SOURCE_FILES})
	target_link_libraries(${TARGET} ${LINK_TARGETS})

	set_target_properties(${TARGET} PROPERTIES
		LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}
		LIBRARY_OUTPUT_NAME ${NAME})
	if(MSVC)
		target_compile_definitions(${TARGET} PRIVATE
			-DNT_PLUGIN
			-DDREQUIRE_IOSTREAM)
	    set_target_properties(${TARGET} PROPERTIES SUFFIX ".mll")
	elseif(APPLE)
		target_compile_definitions(${TARGET} PRIVATE
			-DMAC_PLUGIN
			-DOSMac_
			-DREQUIRE_IOSTREAM)
	    set_target_properties(${TARGET} PROPERTIES PREFIX "" SUFFIX ".bundle")
	else()
		target_compile_definitions(${TARGET} PRIVATE
			-DLINUX
			-DREQUIRE_IOSTREAM)
	    set_target_properties(${TARGET} PROPERTIES PREFIX "")
	endif()
	set_target_properties(${TARGET} PROPERTIES CXX_VISIBILITY_PRESET hidden)

	# Add linker script if present.
	if(LINUX AND EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/exported_symbols_Linux")
		target_link_options(${TARGET} PRIVATE
			"${CMAKE_CURRENT_SOURCE_DIR}/exported_symbols_Linux")
	endif()
	if(APPLE AND EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/exported_symbols_OSX")
		target_link_options(${TARGET} PRIVATE
			"SHELL:-exported_symbols_list ${CMAKE_CURRENT_SOURCE_DIR}/exported_symbols_OSX")
	endif()

	install(TARGETS ${TARGET} LIBRARY DESTINATION ${VERSION}/plug-ins)

	if(APPLE)
		install(CODE "execute_process(COMMAND \"install_name_tool\" \"-change\" \"lib3delight.dylib\" \"/Applications/3Delight/lib/lib3delight.dylib\" \"${CMAKE_INSTALL_PREFIX}/${VERSION}/plug-ins/$<TARGET_FILE_NAME:${TARGET}>\")")
	endif()
endfunction()
