# Locate DirectX root folder.
find_path(DirectX_ROOT_DIR
	NAMES Include/D3DX11.h
	HINTS $ENV{HOME_3DELIGHT}/building/DirectX11
		"C:/Program Files (x86)/Microsoft DirectX SDK (June 2010)"
	REQUIRED)

# Find DirectX include folder.
find_path(DirectX_INCLUDE_DIR
	NAMES D3DX11.h
	HINTS ${DirectX_ROOT_DIR}/Include
	REQUIRED)
set(DirectX_INCLUDE_DIRS "${DirectX_INCLUDE_DIR}")

# Find Directx libraries that this system needs.
set(DirectX_LIBRARIES "")
set(LIBRARY_NAMES
	d3dx11
	d3dcompiler)
foreach(LIBRARY_NAME ${LIBRARY_NAMES})
	find_library(DirectX_${LIBRARY_NAME}_LIBRARY
		NAMES ${LIBRARY_NAME}
		HINTS ${DirectX_ROOT_DIR}/Lib/x64
			${DirectX_ROOT_DIR}/Lib/Win64-x64
		REQUIRED)
	list(APPEND DirectX_LIBRARIES ${DirectX_${LIBRARY_NAME}_LIBRARY})
endforeach()

# Provide target.
find_package_handle_standard_args("DirectX"
	REQUIRED_VARS DirectX_INCLUDE_DIR)

if(DirectX_FOUND AND NOT TARGET DirectX::DirectX)
	add_library(DirectX::DirectX INTERFACE IMPORTED)
	set_target_properties(DirectX::DirectX PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES "${DirectX_INCLUDE_DIRS}"
		INTERFACE_LINK_LIBRARIES "${DirectX_LIBRARIES}")
endif()
