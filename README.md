# Introduction

This repository contains the source to the 3Delight for Maya
plug-in. This is the exact same product you can install from www.3delight.com.

# Development

## Prerequisites

### Windows

- Visual Studio 2017+
- DirectX SDK

### macOS

- macOS 10.15 with Xcode 11.4 or newer

### Linux

- GCC 5 or newer

### CMake

Download and install version 3.25 or later.

### 3Delight

Download and install the 3DelightNSI package. Ensure that the shell in which you will compile 3Delight for Maya has the proper environment to run 3Delight tools like renderdl and oslc. Refer to the 3DelightNSI installation instructions for more details.

### Maya Devkit

For Maya Devkit, please visit the following link: [Maya Devkit](https://www.autodesk.com/developer-network/platform-technologies/maya)

Download and decompress the Devkit. You can either move it to `$MAYA_LOCATION/devkit`, or decompress it to a suitable location. The CMake system will automatically detect them if they are placed in one of the following directories:
- `/Applications/Autodesk/maya${VERSION}`
- `/usr/autodesk/maya${VERSION}`
- `C:/Program Files/Autodesk/Maya${VERSION}`

## Compilation

### CMake Flags

#### CMAKE_INSTALL_PREFIX

If this value is not specified, the default installation prefix is `${CMAKE_BINARY_DIR}/3DelightForMaya`.

#### MAYA_VERSIONS

You must use the CMake variable `MAYA_VERSIONS` to specify the Maya versions you intend to use. For example, you can set `MAYA_VERSIONS` as `2018;2019;2020;2022;2023;2024` for all known Maya versions, or just `MAYA_VERSIONS=2018` for a single version.

#### Maya{VERSION}Devkit_ROOT_DIR

You can use the CMake variable `Maya{VERSION}Devkit_ROOT_DIR` to specify the root directory of the decompressed Maya Devkit for a specific Maya version. If not specified, the build system will attempt to locate it in default locations, such as `C:\Program Files\Autodesk\Maya{VERSION}` for Windows or `/usr/autodesk/maya{VERSION}` for Unix-like systems.

#### USE_XGEN

If XGen support is not required, use the CMake flag `-DUSE_XGEN=OFF` to disable it. The default value is ON. Please note that XGen header files and libraries are not part of the Maya Devkit but are included in the Maya installation. To enable XGen support, a complete Maya installation is necessary.

#### DirectX_ROOT_DIR

For Windows only. The default installation location for the DirectX SDK is `C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)`, but you can specify a custom location if needed.

### How to Build

To build the project, follow these steps:

1. Open the Windows Command Prompt or terminal. Ensure that the `DELIGHT` environment variable is set. If it's not set, you can set it in the Windows Command Prompt with the following command:
   ```
   set DELIGHT="C:\Program Files\3Delight"
   ```
   or, for Unix-like platforms, use:
   ```
   source /usr/local/3delight-2.9.55/.3delight_bash
   ```

2. Clone this repository to a suitable location:
   ```
   git clone https://gitlab.com/3Delight/3DFM.git
   ```

3. Create a clean build directory and navigate to it:
   ```
   mkdir 3DFM-build
   cd 3DFM-build
   ```

4. Assuming that Maya 2018 and 2023 are installed at `C:\Program Files\Autodesk\Maya2018` and `C:\Program Files\Autodesk\Maya2023`, and both Devkit folders have decompressed content, use the following command to build the plugin directly:
   ```
   cmake -DMAYA_VERSIONS="2018;2023" ../3DFM
   ```

5. If the Maya Devkit is decompressed to a customized location, use the `Maya{VERSION}Devkit_ROOT_DIR` variable to specify the location of the Devkit:
   ```
   cmake -DMAYA_VERSIONS="2018;2023" \
       -DMaya2022Devkit_ROOT_DIR=/tmp/Autodesk_Maya_2018_DEVKIT_Linux \
       -DMaya2023Devkit_ROOT_DIR=/tmp/Autodesk_Maya_2023_DEVKIT_Linux \
       ../3DFM
   ```

These instructions should help you set up the development environment and build the project successfully.

# General Design Outlines

An important design principle is to carry as little state as possible. The NSI
API is designed in such a way that outputting the scene in free form, as it
comes and without any special "massaging", is possible and encouraged. This is
particiularely important for Live rendering as any additional scene state
between MAYA and the render engine adds a whole new layer of problems.

The basic unit of scene export is an *export delegate*.  We have one to
export a mesh, another one to export a camera, etc... . Some delegates are
more involved than others but overall its pretty much low complexity code
that takes MAYA nodes and attributes and outputs NSI nodes and attributes.

Some example delegates:

* [NSIExportMesh.cpp](src/NSIExportMesh.cpp)
* [NSIExportLight.cpp](src/NSIExportLight.cpp)
* [NSIExportCamera.cpp](src/NSIExportCamera.cpp)

Above that level, we have a scene parser ([NSIExport.cpp](src/NSIExport.cpp))
that cycles through MAYA nodes, first the Dependency nodes such as shaders and
then the DAG nodes such as meshes, finds the appropriate delegate and perform
the "CSCS" loop.

## The Create/SetAttribute/Connect/SetAtttributeAtTime (CSCS) Loop

This brings us to another design idea in this exporter and it has
to do with scene export effiency for motion blur. Some object attributes, for
example, UV coordinates on a Mesh, should be exported only once for the entire
frame range but other attributes, such as the *position* attribute on the same
mesh might have to be exported at each *motion sample* (more on that [later](#motion-blur)).

To implement the idea, we are helped by NSI API which provides both SetAttribute
and SetAttributeAtTime API calls to modify time varying attributes. So the scene
export is decomposed into 4 steps:

1. Create

    At this point, we create the delegate for each MAYA node. The list of
    delegates is pretty much the only state we carry.
    
2. SetAttribute

    At this point, we cycle through each delegate in the scene and call its
    SetAttibute method which in tuns will generate one or more NSI SetAttribute
    calls.

4. Connect

    We cycle through each delegate and call the `Connect` method. At this point,
    the delegate might interconnect nodes it might have created (in `Create`)
    or connect to some other nodes. A good example, although not for the faint of
    heart, is [NSIExportShadingGroup.cpp](src/NSIExportShadingGroup.cpp)) which
    will connect to all the shading group members at `Connect` time.

3. SetAttributeAtTime

    We cycle through each delegate again, and call SetAttributeAtTime method for
    each time step. The selection of time steps is explained in the [Motion Blur](#motion-blur)
    section.
    
The order of the operation is no that important, we only need the Create
step to be first.

NOTE: This is a high level overview of what happens in NSIExport.cpp. The code
is more complicated (although with enough comments to get you through) because
we need to take care of things related to live render and things related
to how MAYA is designed.

## Motion Blur

Relying on our experience, we tried to present a user-friendly motion-blur interface
to the artist:

* At the highest level, only one [Motion Blur ](https://www.3delight.com/documentation/display/3DFM9/Quality) toggle, ON by default, to enable motion blur.
So by default, the plug-in will do the right thing in most cases.
* Ability to overide the motion oversampling *per transform and per object*. Increasing
motion samples on a transform is handy on effects such as rotations (e.g. wheels,
rotors). Increasing motion samples on an object is handy when using deformers.
* On objects, the default motion blur mode is [ON (if detected)](https://www.3delight.com/documentation/display/3DFM9/Object+Attributes). Which means that
the plug-in will not output additional motion data for non-deforming geometry.
This protects the users against potentially harmfull settings (e.g. high motion
sampling on geometry which doesn't need such a setting).
* No complications: motion always centered on frame.

One interesting idea in the plug-in is how the motion steps are sampled. Say we
have an object with 3 motion samples, another object with 5 samples and another
one with 7 samples. If we are to sample the scene at all the time steps
necessary, we would need 3 + 3 + 5 = 10 motion steps (start and end steps are
the same for all objects). Instead, the plug-in gets the maximum samples asked in
the scene (in this case 7) and "bins" all the other objects in these samples (i.e. quantization).
Even though the steps won't be evenly distributed for some of the objects, this
method still provides satisfactory results and saves some export time.

Implementation is [self explanatory](https://gitlab.com/3Delight/3DFM/blob/662415296640e6909d75fc133de2a8ce60d1a933/src/NSIExport.cpp#L1482).

## Live Render

Live rendering in *3Delight for Maya* is made relatively easy by how NSI works.
As mentioned previously, the secret of a good live rendering implementation
is a one-to-one mapping (or close to) between scene changes and rendering API calls. This
is easier said than done but NSI was designed with this particular aspect in
mind. The difficulty comes from the various gotchas and traps that MAYA is well
known for (have you heard of `__PrenotatoPerDuplicare_`? If not, you are probably
not acquainted -yet?- with live rendering development in MAYA).

In a nutshell, each export delegate can chose to either register callbacks for
its own `MObject` or let a generic handler do the work. The generic handler adds
callbacks for every Maya node for which we have a delegate that doesn't define
its own callbacks. Most delegates do not need specific callbacks and this
makes things easy. The current list of delegates which need its own management
of MAYA callbacks is (you can detect this by the presennce of `virtual RegisterIPRCallbacks`
method in the delegate):

[NSIExportLight.h](src/NSIExportLight.h)

[NSIExportShader.h](src/NSIExportShader.h)

[NSIExportShadingGroup.h](src/NSIExportShadingGroup.h)

[NSIExportPfxGeometry.h](src/NSIExportPfxGeometry.h)

[NSIExportOpenVDB.h](src/NSIExportOpenVDB.h)

The generic callback calls `SetAttributes` and `SetAttributesAtTime` for any delegate
for which a change of attributes has been detected. It also takes care of node
addition (by doing a mini CSCS loop for the added objects) and deletion (by
calling NSIDelete with the recursive flag). This brings us to object renaming
and another important live rendering necessity: all NSI handles are MAYA UUIDs
and not the node names. This allows us to ignore MAYA rename events which would
otherwise need to carry a state (=troubles).

The most involved live rendering operations often concern macro changes such as
changes to the set of objects to render in the [Scene Elements](https://www.3delight.com/documentation/display/3DFM9/Scene+Elements).
In this case, good code compartmentation  saves the day. Here is an example
code snippet that deals with that particular case

~~~c
/**                                                                                     
    \brief A callback on the set that defines the ObjectsToRender                       
                                                                                        
    Note that we don't do anything particular here and we simply re-issue the           
    DAG objects using ReIssueDAGObjects which will issue everything as defined          
    by the .objectsToRender render settings attributes.                                 
                                                                                        
    \see ReIssueDAGObjects                                                              
*/                                                                                      
void NSIExport::ObjectsToRenderCB(                                                      
    MNodeMessage::AttributeMessage i_msg,                                               
    MPlug &/*i_plug*/,                                                                  
    MPlug &/*i_otherPlug*/,                                                             
    void *i_data )                                                                      
{                                                                                       
    if( !(i_msg & (                                                                     
            MNodeMessage::kConnectionMade |                                             
            MNodeMessage::kConnectionBroken)) )                                         
    {                                                                                   
        NSIExport* nsi_export = (NSIExport*) i_data;                                    
        nsi_export->ReIssueDAGObjects( MFn::kGeometric );                               
                                                                                        
        NSI::Context nsi( nsi_export->m_context );                                      
        nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );                
    }                                                                                   
}
~~~


## Light Linking

MAYA's Light linking is particularely "interesting" in the way it operates and
this section will need to cover some more advanced features of NSI, so get yourself
a coffee.

A smooth introduction would be to explain how MAYA's *Illuminate by Default* works
and how it is implemented using NSI. 

If you were writing an exporter for some other renderer than 3Delight,
you would probably have to set some specific attribute on some light object to implement
this feature. For the live render you would monitor any change to the 
`.illuminteByDefault` attribute on the light and act accordingly. This is also
possible in NSI but we can do better.

The *Illuminate by Default" toggle, present on every MAYA light, adds  or removes
a light from the DefaultLightSet (an `MFn::kSet`): if a light is in that set, it is ON. It is OFF
otherwise. This lends itself to a *natural* implementation in NSI using two
 features: attribute inheritance and attribute priorities.

1. Export the Light as usual and create a default attribute node for it. That
attribute node has its 'visibility' set to 0 as to make the light OFF by default.
Here is a [snippet](https://gitlab.com/3Delight/3DFM/blob/f822f80cadbb67e3ea75547d4b5ee9aa6cdb30d9/src/NSIExportLight.cpp#L197):

```c
nsi.SetAttribute( attributes_handle.asChar(),
	(
	    NSI::IntegerArg("visibility", 0),
		NSI::IntegerArg("visibility.priority", NSI_LIGHT_PRIORITY)
	)

```

2. Export the DefaultLightSet node, as we do with any other MAYA node, as a NSI attribute
node and set its `visibility` to 1 and `priority` to some higher value. The exporter
of the default light set, in [NSIExportDefaultLightSet.cpp](src/NSIExportDefaultLightSet.cpp).

```c
nsi.SetAttribute( Handle(),
	(
		NSI::IntegerArg("visibility", 1),
		NSI::IntegerArg("visibility.priority", NSI_LIGHTSET_PRIORITY)
	) );
```

(NSI_LIGHTSET_PRIORITY is a number larger than NSI_LIGHT_PRIORITY).

And that's it. But how will that work ? By usual scene export mechanicis: when
exporting the default light set, and executing the `Connect` method during the
CSCS loop, the delegate will [connect](https://gitlab.com/3Delight/3DFM/blob/f822f80cadbb67e3ea75547d4b5ee9aa6cdb30d9/src/NSIExportDefaultLightSet.cpp#L28)
itself ("itself" being the NSI attribute that represents the default light set)
to every light in the set. This will override  light's default OFF visiblity by
inheriting the *visibility* attribute with a higher *priority*.

This way of doing things, following the logic of the englobing architecture and
implementing it using the rendering API, is an advantage as it allows
to build more robust systems that integrates naturrally.

... TO BE CONTINUED ...

# Directory Structure

WIP

## src/

Contains the C++ sources of the exporter. Here is a quick overview:

1.  `src/NSIExport*{cpp,h}`
    
    These files contains the main export logic. NSIExport.cpp contains the main
    export loop while files likes NSIExportMesh.{cpp,h} constain the NSI calls
    to export geometry.

2.  `src/dl*.{cpp,h}`
    
    These files contain the 3Delight for Maya nodes definitions. For example,
    dl3DelightMaterial.{cpp,h} is responsible of creating the 3Delight Material
    along with it's attributes.

3.  `src/DL_*.{cpp,h}`
    
    This a a legacy nomenclature that usually signifies a utilty file ported
    from the RenderMan days. A good example is DL_utils.{cpp,h} which contains
    different  Maya utility functions.

## mel/

Contains many utility functions, templates for the Attribute Editor, Maya shelve
and menu mangement, UI utilities, etc ... 


# How You Can Help

* Help us closing issues.

* Help us with testing.


