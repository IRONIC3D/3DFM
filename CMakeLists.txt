#
# 3Delight for Maya CMakeList
#
cmake_minimum_required(VERSION 3.25)
project(3Delight_for_Maya LANGUAGES CXX)

# Set CMAKE_CXX_STANDARD to use C++17 for all C++ targets.
set(CMAKE_CXX_STANDARD 17)

# Disable 'Up-to-date' install messages.
set(CMAKE_INSTALL_MESSAGE LAZY)

# Include CMake modules.
include(FindPackageHandleStandardArgs)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# Set default install prefix for CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT.
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/3DelightForMaya"
		CACHE PATH "..."
		FORCE)
	set(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT FALSE)
endif()

# Build with static CRT on Windows.
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")

# Platform specific configurations.
if(MSVC)
	add_definitions(-DNOMINMAX)
	add_definitions(-DWIN32_LEAN_AND_MEAN)
	add_compile_options(/GS-)
elseif(UNIX)
	# Set CMAKE_BUILD_TYPE for Linux and macOS.
	# Visual Studio or Xcode doens't need this.
	if(CMAKE_GENERATOR STREQUAL "Unix Makefiles")
		if(CMAKE_BUILD_TYPE STREQUAL "")
			set(CMAKE_BUILD_TYPE "Release")
		endif()
	endif()

	# Skip RPATH.
	set(CMAKE_SKIP_INSTALL_RPATH ON)
	set(CMAKE_SKIP_RPATH ON)
endif()

# Set needed MAYA_VERSIONS.
set(MAYA_VERSIONS "" CACHE STRING "2018;2022;2023;2024")
if(MAYA_VERSIONS STREQUAL "")
	message(FATAL_ERROR "No MAYA_VERSIONS")
endif()

# Find 3Delight, check cmake/Find3Delight.cmake for more.
find_package(3Delight REQUIRED)

# Find MayaDevkit, check cmake/FindMayaDevkit.cmake for more.
foreach(MAYA_VERSION ${MAYA_VERSIONS})
	find_package(MayaDevkit ${MAYA_VERSION} REQUIRED)
endforeach()

# Find DirectX for Windows only.
if(MSVC)
	find_package(DirectX REQUIRED)
endif()

# Compile subdirectories.
option(USE_XGEN "Build XGen support" ON)

if(USE_XGEN)
	# Find XGen for every Maya version.
	foreach(MAYA_VERSION ${MAYA_VERSIONS})
		find_package(XGen ${MAYA_VERSION} REQUIRED)
	endforeach()

	# Process folder.
	add_subdirectory(3dfmXGen)
endif()

add_subdirectory(icons)
add_subdirectory(mel)
add_subdirectory(modules)
add_subdirectory(osl)
add_subdirectory(presets)
add_subdirectory(render_desc)
add_subdirectory(shaderFragments)
add_subdirectory(src)
add_subdirectory(swatch)
add_subdirectory(xmltemplates)
