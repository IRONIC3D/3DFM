#ifndef __dlHairAndFur_H__
#define __dlHairAndFur_H__

#include <maya/MPxNode.h>

class dlHairAndFur : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	static MObject s_color;
	static MObject s_outColor;
};

#endif
