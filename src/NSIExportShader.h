#ifndef __NSIExportShader_h
#define __NSIExportShader_h

#include "NSIExportDelegate.h"
#include "NSIExportUtilities.h"

#include <nsi.hpp>

#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MPlug;
#endif

struct ParamData;
struct ShaderData;

class NSIExportShader : public NSIExportDelegate
{
public:
	NSIExportShader(
		MObject i_object,
		const NSIExportDelegate::Context& i_context );

	~NSIExportShader();

	virtual void Create();
	virtual void SetAttributes();
	virtual void SetAttributesAtTime( double i_time, bool i_no_motion );
	virtual void Connect( const DelegateTable* );

	virtual bool RegisterCallbacks();

	bool Exist() const;

	static void CreateTextureUVCoordNode( NSIContext_t i_nsi );
	static MString NSITextureUVCoordHandle();

	MString NSIUVCoordHandle();

private:

	/**
		\brief Returns the shader parameter name as a MString, including the
		possible component index.

		This should probably only used when needed a shader parameter name for
		a connection, since OSL does not allow setting a single component value.
		If you don't need the component, then don't call this and use
		ParamData.m_name directly.

		A negative value for i_componentIndex indicates that no '[n]' will be
		added to i_paramData->m_name.

		\param i_paramData The shader parameter cached data.

		\param i_componentIndex The index of the parameter component, or -1
			to designate the shader parameter itself.
	*/
	MString GetParamName( const ParamData* i_paramData, int i_componentIndex );

	/**
		\brief Returns the cached ParamData for the shader parameter related to
		a given plug, for a connection handling context.

		The difference for the connection handling context is that the function
		can handle cases where i_plug is a child of a compoound attr (e.g. colorR)
		and will try to match it to a shader parameter component if the component
		itself is not declared specifically as a shader parameter. In that case,
		the parameter component is set in o_componentIndex.

		\param i_plug The attribute to look for a related shader parameter.

		\param i_ioType Defines if we are considering this attr as an input or an
			output attribute.

		\param i_shaderData The optional cached shader data for i_plug.node(). When
			set to NULL, the function will use an internally generated one.

		\param o_componentIndex If i_plug is matched to a compoment of the returned
			ParamData*, this is set to the matched component index.
	*/
	const ParamData* GetParamDataForConnectedAttr(
		const MPlug& i_plug,
		NSIExportUtilities::IoType i_ioType,
		const ShaderData* i_shaderData,
		int& o_componentIndex );

	/**
		\brief Returns the shading node of the incoming connection on i_plug.

		Inspect the incoming connections on i_plug and return the MObject of the
		upstream node if it is a supported shading node, and if its connected
		attribute corresponds to a shader parameter.

		The returned MObject will be null (MObject::isNull() == true) if the above
		condition is not met.

		The return value along with with o_otherPlugPramName provides the
		necessary info for a NSI Connect statement.

		\param i_plug The Maya plug to check incoming connections on.

		\param o_otherPlugParamName Will be set to the shader parameter name that
			corresponds to the connected plug on the returned MObject.
	*/
	MObject GetConnectedShadingNode(
		const MPlug& i_plug,
		MString& o_otherPlugParamName );

	/**
		\brief Add one shading node attribute as a NSI Argument in the provided
			ArgList.
	*/
	const ParamData* SetOneShaderAttribute(
		const MPlug& i_plug,
		const ShaderData* i_shaderData,
		NSI::ArgumentList& o_argList );

	/**
		\brief Edit the value of one shader attribute during a live render session.
	*/
	void EditOneShaderAttribute( const MPlug& i_plug );

	void EditOneShaderConnection(
		const MPlug& i_plug,
		const MPlug& i_otherPlug,
		bool i_connectionMade );

	/**
		\brief Indicates the need to complete a bump value connection.

		\param i_paramName
			Input parameter of the current shader that is being processed.

		Maya doesn't provide an explicit plug to connect the bump normal on the
		"bump2d" shader. Instead, the output color of the node which provides
		the input for the scalar bump value is automatically used under the
		hood. This function returns true when the shader is a "bump2d" and
		i_paramValue is "bumpValue" so that the additional connection could be
		handled properly.
	*/
	bool IsBump2DValue(const MString& i_paramName)const;

	static void AttributeChangedCB(
		MNodeMessage::AttributeMessage i_msg,
		MPlug& i_plug,
		MPlug& i_otherPlug,
		void* i_delegate);

	static void NodeDirtyCB(MObject&, MPlug& i_plug, void* i_delegate);

	bool IsPartOfEnvironmentShader( MObject &_this, MObject &o_node );

	bool m_exist;
};

#endif
