#ifndef __NSIExportSet_h
#define __NSIExportSet_h

#include "NSIExportDelegate.h"

#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MPlug;
#endif

/**
	\sa NSIExportDelegate
*/
class NSIExportSet : public NSIExportDelegate
{
public:
	NSIExportSet(
		MFnDependencyNode &,
		MObject &,
		const NSIExportDelegate::Context& i_context );

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable * );

	static void AttributeChangeCallback(
		MNodeMessage::AttributeMessage i_msg,
		MPlug& i_plug,
		MPlug& i_other_plug,
		void* i_data );

private:

	MString NSIDlSetAttributesHandle( const MObject&,
		const char *i_enable_attr );

	void AdjustAttributeConnection(const char* i_enable_attr);
};
#endif
