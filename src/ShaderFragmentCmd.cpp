#include "ShaderFragmentCmd.h"

#include <maya/MArgParser.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MItSelectionList.h>
#include <maya/MSelectionList.h>
#include <maya/MString.h>
#include <maya/MSyntax.h>

#include "DL_errors.h"
#include "DL_utils.h"
#include "VPFragmentRegistry.h"
#include "VPUtilities.h"

MSyntax ShaderFragmentCmd::newSyntax()
{
	MSyntax syntax;

	/*
		The following options are derived from the dumpFragment example from the
		Maya dev kit:
		-dump
		-includeUpstream
		-objectContext

		This is thought of as a shader development tool, especially given the lack
		of complete Maya documentation for	the shader fragments - the doc
		recommends	to inspect the shading fragments shipped with Maya using that
		example command.
	*/

	syntax.addFlag( "-af", "-addFragment", MSyntax::kString );
	syntax.addFlag( "-afg", "-addFragmentGraph", MSyntax::kString );
	syntax.addFlag( "-ap", "-addPath", MSyntax::kString );
	syntax.addFlag( "-d", "-dump", MSyntax::kString );
	syntax.addFlag( "-eod", "-effectOutputDir", MSyntax::kString );
	syntax.addFlag( "-e", "-exists", MSyntax::kString );
	syntax.addFlag( "-god", "-graphOutputDir", MSyntax::kString );
	syntax.addFlag( "-iu", "-includeUpstream" );
	syntax.addFlag( "-l", "-list" );
	syntax.addFlag( "-oc", "-objectContext", MSyntax::kString );
	syntax.addFlag( "-rl", "-reload", MSyntax::kString );
	syntax.addFlag( "-r", "-removeFragment", MSyntax::kString );
	syntax.addFlag( "-v", "-verbose", MSyntax::kUnsigned );

	syntax.enableQuery( false );
	syntax.enableEdit( false );

	return syntax;
}

MStatus ShaderFragmentCmd::doIt( const MArgList& i_argList )
{
	MStatus status;
	MArgParser argParser( syntax(), i_argList, &status );

	if( status != MStatus::kSuccess )
	{
		return status;
	}

	bool isAddSet = argParser.isFlagSet( "-addFragment" );
	bool isAddGraphSet = argParser.isFlagSet( "-addFragmentGraph" );
	bool isAddPathSet = argParser.isFlagSet( "-addPath" );
	bool isDumpSet = argParser.isFlagSet( "-dump" );
	bool isEffectDirSet = argParser.isFlagSet( "-effectOutputDir" );
	bool isExistsSet = argParser.isFlagSet( "-exists" );
	bool isGraphDirSet = argParser.isFlagSet( "-graphOutputDir" );
	bool isIncludeUpstreamSet = argParser.isFlagSet( "-includeUpstream" );
	bool isListSet = argParser.isFlagSet( "-list" );
	bool isObjectContextSet = argParser.isFlagSet( "-objectContext" );
	bool isReloadSet = argParser.isFlagSet( "-reload" );
	bool isRemoveSet = argParser.isFlagSet( "-removeFragment" );
	bool isVerboseSet = argParser.isFlagSet( "-verbose" );

	unsigned int numFlagsSet = argParser.numberOfFlagsUsed();

	// Validate the flag usage
	int numMandatoryFlags = isAddSet + isAddGraphSet + isAddPathSet + isDumpSet +
		isEffectDirSet + isExistsSet + isGraphDirSet + isListSet + isReloadSet +
		isRemoveSet + isVerboseSet;

	if( numMandatoryFlags != 1 )
	{
		MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
			MString( "precisely one of -addFragment, -addFragmentGraph, " ) +
			MString( "-addPath, -dump, -effectOutputDir , -exist, " ) +
			MString( "-graphOutputDir -list, -reload, -removeFragment " ) +
			MString( "or -verbose is required." );

		displayError( error );
		return MStatus::kFailure;
	}

	if( isAddPathSet )
	{
		MString path;
		argParser.getFlagArgument( "-addPath", 0, path );
		VPFragmentRegistry::Instance()->AddPath( path );
		return MStatus::kSuccess;
	}

	if( isListSet )
	{
		MStringArray list;
		VPUtilities::GetFragmentList( list );
		setResult( list );
		return MStatus::kSuccess;
	}

	if( isAddSet || isAddGraphSet )
	{
		MFileObject file;
		file.setRawPath( VPFragmentRegistry::Instance()->SearchPath() );
		file.setResolveMethod( MFileObject::kInputFile );

		MString fragmentName;

		if( isAddSet )
		{
			argParser.getFlagArgument( "-addFragment", 0, fragmentName );
		}
		else
		{
			argParser.getFlagArgument( "-addFragmentGraph", 0, fragmentName );
		}

		bool result = VPFragmentRegistry::Instance()->AddFragment(
			fragmentName,
			isAddGraphSet,
			file );

		setResult( result );

		return result ? MStatus::kSuccess : MStatus::kFailure;
	}

	if( isRemoveSet )
	{
		MString fragmentName;
		argParser.getFlagArgument( "-removeFragment", 0, fragmentName );

		bool success =
			VPFragmentRegistry::Instance()->RemoveFragment( fragmentName );

		if( success )
		{
			VPUtilities::ClearShaderMgrEffectCache();
		}

		setResult( success );
		return success ? MStatus::kSuccess : MStatus::kFailure;
	}

	if( isExistsSet )
	{
		MString fragmentName;
		argParser.getFlagArgument( "-exists", 0, fragmentName );

		setResult( VPUtilities::FragmentExists( fragmentName ) );
		return MStatus::kSuccess;
	}

	if( isVerboseSet )
	{
		unsigned verbosity = 0;
		argParser.getFlagArgument( "-verbosity", 0, verbosity );
		VPFragmentRegistry::Instance()->Verbosity( verbosity );
		return MStatus::kSuccess;
	}

	if( isDumpSet )
	{
		// With -dump, the argument can be either a fragment name or a shading node
		MString argument;
		argParser.getFlagArgument( "-dump", 0, argument );

		MString fragmentName = argument;

		if( VPUtilities::FragmentExists( fragmentName ) )
		{
			// Dump the single fragment specified as argument.
			MString buffer;
			MStatus status = VPUtilities::GetFragmentContent(
				fragmentName,
				buffer );

			setResult( buffer );

			if( status != MStatus::kSuccess )
			{
				MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
					MString( "Fragment '" ) + fragmentName +
					MString ("' exists but its content is not readable.");

				displayError( error );
			}
			return status;
		}

		// Argument is not a fragment, so assume it is a Maya object.
		MSelectionList selectionList;
		selectionList.add( argument );
		MItSelectionList iter( selectionList );

		MObject shadingNode;
		if( iter.getDependNode( shadingNode ) != MS::kSuccess )
		{
			MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) + "'" +
				argument + "' designates neither a known fragment nor a shading node.";

			displayError( error );
			return MS::kFailure;
		}

		MDagPath objContextPath;
		MDagPath* objContextPathPtr = 0x0;

		// Attempt to figure the MObject that was specificed as an argument
		if( isObjectContextSet )
		{
			argParser.getFlagArgument( "-objectContext", 0, argument );

			selectionList.clear();
			selectionList.add( argument );

			MItSelectionList iter( selectionList );

			if( iter.getDagPath( objContextPath ) != MS::kSuccess ||
				!objContextPath.isValid() )
			{
				MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) + "'" +
					argument + "': object not found.";

				displayError( error );
				return MS::kFailure;
			}

			objContextPath.extendToShape();
			objContextPathPtr = &objContextPath;
		}

		MString buffer;
		MStatus status = VPUtilities::GetFragmentContent(
			shadingNode,
			buffer,
			isIncludeUpstreamSet,
			objContextPathPtr );

		if( status != MStatus::kSuccess )
		{
			MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
				MString( "Could not find any fragment for '" + argument + "'" );

			displayError( error );
			return status;
		}

		setResult( buffer );
		return status;
	}

	if( isReloadSet )
	{
		// Attempt to figure the MObject that was specificed as an argument
		MString argument;
		argParser.getFlagArgument( "-reload", 0, argument );

		MSelectionList selectionList;
		selectionList.add( argument );
		MItSelectionList iter( selectionList );

		MObject shadingNode;
		if( iter.getDependNode( shadingNode ) != MS::kSuccess )
		{
			MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) + "'" +
				argument + "': object not found.";

			displayError( error );
			return MS::kFailure;
		}

		MFnDependencyNode depFn( shadingNode );
		MStringArray classes;
		Utilities::GetTypeClassificationStrings( depFn.typeName(), classes );

		MString vpShaderClass = VPUtilities::GetVPShaderOverrideClass();
		MString vpSurfaceClass = VPUtilities::GetVPSurfaceShaderOverrideClass();

		bool isVPShader = Utilities::HasClass( vpShaderClass,	classes );
		bool isVPSurfaceShader = false;
		if( !isVPShader )
		{
			isVPSurfaceShader = Utilities::HasClass( vpSurfaceClass, classes );
		}

		if( !isVPShader && !isVPSurfaceShader )
		{
			MString error = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
				MString( "Cannot reload the shader fragments of '") +
				depFn.name() +
				MString("' because that node type uses neither ") +
				vpShaderClass +
				MString (" nor ") +
				vpSurfaceClass +
				MString(" classifications.");

			displayError( error );
			return MStatus::kFailure;
		}

		/*
			In order for Maya to re-read the fragments and refresh the viewport
			using the updated version, it is necessary to :
			1- de-register the VP shader override class
			2- remove the fragments
			3- clear the effect cache
			4- register the VP shader override class again
			5- add the fragments.

			Skipping any of 1, 2 or 3 above will not refresh the viewport.
		*/
		if( isVPShader )
		{
			VPUtilities::DeregisterVPShaderOverride();
		}
		else
		{
			VPUtilities::DeregisterVPSurfaceShaderOverride();
		}

		VPFragmentRegistry::Instance()->Remove( depFn.typeName() );
		VPUtilities::ClearShaderMgrEffectCache();

		if( isVPShader )
		{
			VPUtilities::RegisterVPShaderOverride();
		}
		else
		{
			VPUtilities::RegisterVPSurfaceShaderOverride();
		}

		MStringArray fragments;
		VPFragmentRegistry::Instance()->Add( depFn.typeName(), fragments );

		if( fragments.length() > 0 )
			return MStatus::kSuccess;

		return MStatus::kFailure;
	}

	if( isEffectDirSet )
	{
		MString path;
		argParser.getFlagArgument( "-effectOutputDir", 0, path );

		if( path.length() > 0 )
		{
			path = path.expandFilePath() + "/";
		}

		VPUtilities::SetEffectOutputDir( path );

		MString msg = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
			"Effect output dir set to \"" + path + "\". ";

		if( path.length() > 0 )
		{
			msg += MString( "Performance will be degraded. " ) +
				"Set the path to \"\" to restore performance.";
			MGlobal::displayWarning( msg );
		}
		else
		{
			MGlobal::displayInfo( msg );
		}

		return MStatus::kSuccess;
	}

	if( isGraphDirSet )
	{
		MString path;
		argParser.getFlagArgument( "-graphOutputDir", 0, path );

		if( path.length() > 0 )
		{
			path = path.expandFilePath() + "/";
		}

		VPUtilities::SetGraphOutputDir( path );

		MString msg = _3DFM_ERR_PREFIX( SHADERFRAGMENTCMD_STR ) +
			"Intermeditate fragment graph output dir set to \"" + path + "\". ";

		if( path.length() > 0 )
		{
			msg += MString( "Performance will be degraded. " ) +
				"Set the path to \"\" to restore performance.";
			MGlobal::displayWarning( msg );
		}
		else
		{
			MGlobal::displayInfo( msg );
		}

		return MStatus::kSuccess;
	}

	return MStatus::kSuccess;
}
