/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DL_FILESEQUENCE_H__
#define __DL_FILESEQUENCE_H__

#include <maya/MObject.h>
#include <maya/MGlobal.h>

//
// Simple structure to hold attributes that define a file sequence.
// A node that needs these attributes should:
//   - declare a static member of this struct;
//   - call DL_fileSequence::initialize() somewhere inside its own initialize();
//   - still during initialize(), declare output plug dependencies to the file
//     file sequence attributes with proper calls to attributeAffects() - this
//     is why the MObject attributes are public here.
//
struct DL_fileSequence
{
public:
	// Init & add thge file sequence attributes.
	// This function should be called from within MPxNode::initialize()
	MStatus initialize();

	// Returns the file sequence number based on the provided current frame, and
	// the various file sequence attribute values.
	//
	// When m_fileSequenceMode is set to 0, the returned index is the rounded
	// value of i_currentFrame.
	//
	int getSequenceNumber(MObject i_obj, double i_currentFrame);

	// Attribute objects - public so that they can be easily used for 
	// attributeAffects() calls in MPxNode::initialize().
	//
public:
	MObject m_fileSequenceMode;
	MObject m_firstIndex;
	MObject m_lastIndex;
	MObject m_offset;
	MObject m_increment;
	MObject m_outsideRangeMode;
	MObject m_frameRangeMode;
	MObject m_frameRangeStart;
	MObject m_frameRangeEnd;
};

#endif
