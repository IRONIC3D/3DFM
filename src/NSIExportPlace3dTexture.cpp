#include "NSIExportPlace3dTexture.h"

NSIExportPlace3dTexture::NSIExportPlace3dTexture(
	MObject i_object, const NSIExportDelegate::Context& i_context )
:
	NSIExportShader( i_object, i_context )
{
}

void NSIExportPlace3dTexture::SetAttributes()
{
	NSIExportShader::SetAttributes();

	/*
		This is the reason we need this exporter ! We need to set the space
		of the shader to the name of the transform.
	*/
	MString transform = NSIHandle( Object(), m_live );

	NSI::Context nsi( m_nsi );
	nsi.SetAttribute( Handle(), NSI::CStringPArg("space", transform.asChar()) );
}
