#ifndef __DLSET_H__
#define __DLSET_H__

#include <maya/MFnAttribute.h>
#include <maya/MPxObjectSet.h>


class dlSet : public MPxObjectSet
{
public:
	virtual MStatus compute( const MPlug& plug, MDataBlock& data);

	virtual void postConstructor();

	static void* creator();
	static MStatus initialize();

	static MTypeId m_id;

private:
	static MObject m_enablePolyAsSubd;
	static MObject m_polyAsSubd;

	static MObject m_enableMatte;
	static MObject m_matte;

	static MObject m_enablePrelit;
	static MObject m_prelit;

	static MObject m_enableCompositingMode;
	static MObject m_compositingMode;

	static MObject m_enableExtraTransformSamples;
	static MObject m_extraTransformSamples;

	static MObject m_enableExtraDeformSamples;
	static MObject m_extraDeformSamples;

	static MObject m_enableSmoothCurves;
	static MObject m_smoothCurves;

	static MObject m_enableCameraVisibility;
	static MObject m_cameraVisibility;

	static MObject m_enableDiffuseVisibility;
	static MObject m_diffuseVisibility;

	static MObject m_enableReflectionVisibility;
	static MObject m_reflectionVisibility;

	static MObject m_enableRefractionVisibility;
	static MObject m_refractionVisibility;

	static MObject m_enableShadowVisibility;
	static MObject m_shadowVisibility;
};

#endif
