#include <maya/MArgList.h>
#include <maya/MArgParser.h>
#include <maya/MSyntax.h>
#include <maya/MGlobal.h>

#include "DL_existsCmd.h"
#include "DL_mstringSTLutils.h"

#include <map>


typedef std::map<MString, bool, delight_mstringLessThan> tMStringBoolMap;
static tMStringBoolMap existsMap;
static tMStringBoolMap userExistsMap;

MSyntax
DL_existsCmd::newSyntax()
{
  MSyntax syntax;
  
  syntax.addFlag("-u", "-user");
  syntax.addFlag("-f", "-flush");
  
  syntax.addArg(MSyntax::kString);
  return syntax;
}

MStatus
DL_existsCmd::doIt(const MArgList& i_args)
{
  MStatus status(MStatus::kSuccess);
  MArgParser arg_parser(syntax(), i_args, &status);
  
  if(status != MStatus::kSuccess)
    return MStatus::kFailure;
  
  bool user_flag = arg_parser.isFlagSet("-u");
  bool flush_flag = arg_parser.isFlagSet("-f");
  
  if(flush_flag)
  {
    if(user_flag)
      userExistsMap.clear();
    else
      existsMap.clear();
  }
  else
  {
    bool proc_exists = false;
    
    MString proc_name;
    status = arg_parser.getCommandArgument(0, proc_name);
    
    if(status != MStatus::kSuccess)
      return status;
    
    tMStringBoolMap* map = &existsMap;
    
    if(user_flag)
      map = &userExistsMap;
      
    tMStringBoolMap::iterator itr(map->find(proc_name));
    if(itr != map->end())
      proc_exists = (*itr).second;
    else
    {
      MString cmd("exists(\"");
      cmd += proc_name + MString("\")");
      
      int exists = 0;
      status = MGlobal::executeCommand(cmd, exists);
      if(status == MStatus::kSuccess)
      {
        proc_exists = static_cast<bool>(exists);
        std::pair<MString, bool> map_elem(proc_name, proc_exists);
        map->insert(map_elem);
      }
    }
    setResult(proc_exists);
  }
  
  return status;
}
