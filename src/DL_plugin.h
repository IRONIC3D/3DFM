#ifndef __DL_PLUGIN_H__
#define __DL_PLUGIN_H__

// Because we need MAYA_API_VERSION
#include <maya/MTypes.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MFnPlugin;
class MString;
class MTypeId;
class MSyntax;
#endif

class DL_plugin
{
public:
  // The following are wrappers around MFnPlugin functions that are needed
  // outside of DL_plugin.cpp. This is one way of avoiding issues caused by
  // multiple inclusions of MFnPlugin.h.
  //
	static MStatus registerShape(
		MFnPlugin& i_plugin,
		const MString& i_typeName,
		const MTypeId& i_typeId,
		MCreatorFunction i_creatorFunction,
		MInitializeFunction i_initFunction,
		MCreatorFunction i_uiCreatorFunction,
		const MString* i_classification = NULL);
	
	static MStatus deregisterNode(MFnPlugin& i_plugin, const MTypeId& i_typeId);
  
  static MStatus registerCommand(
    MFnPlugin& i_plugin,
    const MString& i_commandName,
    MCreatorFunction i_creatorFunction,
    MCreateSyntaxFunction i_createSyntaxFunction);
    
  static MStatus deregisterCommand(
    MFnPlugin& i_plugin, 
    const MString& i_commandName);

  static MString name();
  static void setName(MString i_name);

private:
  static MString m_pluginName;
};

#endif
