#include "DL_nsiArchiveTranslator.h"

#include <maya/MGlobal.h>

DL_nsiArchiveTranslator::DL_nsiArchiveTranslator()
{
}

DL_nsiArchiveTranslator::~DL_nsiArchiveTranslator()
{
}

void* DL_nsiArchiveTranslator::creator()
{
	return new DL_nsiArchiveTranslator();
}

MStatus DL_nsiArchiveTranslator::writer(
	const MFileObject& i_file,
	const MString& i_options,
	FileAccessMode i_mode)
{
	MString cmd("NAT_export(");
	cmd += MString("\"") + i_file.rawFullName() + MString("\", ");
	cmd += MString("\"") + i_options + MString("\", ");
	cmd += (i_mode == kExportActiveAccessMode ? "1" : "0") + MString(")");

	return MGlobal::executeCommand(cmd);
}

bool DL_nsiArchiveTranslator::haveReadMethod() const
{
	return false;
}

bool DL_nsiArchiveTranslator::haveWriteMethod() const 
{
	return true;
}

MString DL_nsiArchiveTranslator::defaultExtension() const
{
	return MString("nsi");
}
