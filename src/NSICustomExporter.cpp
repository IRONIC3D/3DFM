/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "NSICustomExporter.h"

MString NSIUtils::NSIHandle( MObject i_node )
{
	// The actual implementation is in class NSIUtilsImp
	return MString();
}

MString NSIUtils::NSIAttributesHandle( MObject i_node )
{
	// The actual implementation is in class NSIUtilsImp
	return MString();
}

MString NSIUtils::NSIShaderHandle( MObject i_node )
{
	// The actual implementation is in class NSIUtilsImp
	return MString();
}
