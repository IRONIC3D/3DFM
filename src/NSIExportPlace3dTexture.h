#pragma once

#include "NSIExportDelegate.h"
#include "NSIExportUtilities.h"
#include "NSIExportShader.h"

#include <nsi.hpp>

#include <maya/MNodeMessage.h>

/**
	\brief Exports a kPlace3dTexture node.

	The reason this is not exported as a simple shader: we need to set the
	space of the matrix properly. And that depends on a transform. The name
	of the transform is the name of the parent Maya dag node (a transform)
*/
class NSIExportPlace3dTexture : public NSIExportShader
{
public:
	NSIExportPlace3dTexture(
		MObject i_object,
		const NSIExportDelegate::Context& i_context );
	virtual void SetAttributes();
};
