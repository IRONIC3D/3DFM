#ifndef __NSIExportMaterialViewerLight_h
#define __NSIExportMaterialViewerLight_h

#include "NSIExportDelegate.h"

#include <maya/MStatus.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MObject;
class MPlugArray;
#endif

/**
	\sa NSIExportDelegate
*/
class NSIExportMaterialViewerLight : public NSIExportDelegate
{
	enum LightType
	{
		e_spot,
		e_distant,
		e_area,
		e_invalid
	};

public:
	NSIExportMaterialViewerLight(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context );
	~NSIExportMaterialViewerLight();
	virtual const char *NSINodeType( void ) const;
	virtual void Create( void );
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable * );

private:
	MString InstallationPath( const char * );

private:
	LightType m_type;
};

#endif
