#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MDrawData.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>

#include <map>
#include <vector>

#include "OpenVDBCache.h"
#include "DL_mstringSTLutils.h"
#include "DL_utils.h"
#include "VDBQuery.h"

/**
*/
struct OpenVDBVertices
{
	OpenVDBVertices(const char *i_filename);
	void ReadMetadata(const char* i_filename);
	void ReadVertices(const char * i_grid);

	std::vector<std::string> m_grids;

	std::string m_filename;
	std::string m_loaded_grid;
	std::vector<float> m_vertices;
	std::vector<float> m_colors;
};

/* Cache of all the VDB files. */
typedef std::map<MString, OpenVDBVertices*, delight_mstringLessThan>
	t_VDBInfoCache;

static t_VDBInfoCache& GetVDBInfoCache()
{
	static t_VDBInfoCache cache;
	return cache;
}

/**
	\brief A create-on-read OpenVDBVertices access.
*/
const OpenVDBVertices& ReadCachedVertices(
	const char *i_filename, const char *i_grid = 0x0)
{
	OpenVDBVertices* vdbstat;
	t_VDBInfoCache::const_iterator it = GetVDBInfoCache().find(i_filename);
	if( it != GetVDBInfoCache().end() )
	{
		vdbstat = it->second;
	}
	else
	{
		/* Not found. Create a new one */
		vdbstat = new OpenVDBVertices(i_filename);
		GetVDBInfoCache()[i_filename] = vdbstat;
	}

	if( i_grid )
	{
		vdbstat->ReadVertices( i_grid );
	}

	return *vdbstat;
}


OpenVDBVertices::OpenVDBVertices(const char* i_filename)
:
	m_filename(i_filename)
{
	m_loaded_grid.clear();
	m_vertices.clear();
	m_colors.clear();

	ReadMetadata( i_filename );
}

void OpenVDBVertices::ReadMetadata(const char *i_filename )
{
	int num_grids = 0;
	const char *const *grid_names;
	if( DlVDBGetGridNames( i_filename, &num_grids, &grid_names ) )
	{
		for( int i = 0; i < num_grids; ++i )
		{
			m_grids.emplace_back( grid_names[i] );
		}
		DlVDBFreeGridNames( grid_names );
	}
}

void OpenVDBVertices::ReadVertices(const char *i_grid)
{
	/* Don't reread the same grid. */
	if( m_loaded_grid == i_grid )
		return;

	m_loaded_grid = i_grid;

	size_t num_points;
	const float *points;
	DlVDBGeneratePoints(
		m_filename.c_str(),
		i_grid,
		&num_points,
		&points);

	m_vertices.assign(points, points + 3 * num_points);
	/* No actual color support for now. */
	m_colors.resize(3 * num_points, 1.0f);

	DlVDBFreePoints(points);
}

MSyntax OpenVDBCache::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("-cl", "-channelList");
	syntax.addFlag("-f", "-flush");
	syntax.setObjectType(MSyntax::kStringObjects, 0, 2);
	return syntax;
}

MStatus OpenVDBCache::doIt( const MArgList& args )
{
	MStatus status;
	MArgDatabase argData(syntax(), args, &status);

	if(status != MS::kSuccess)
	{
		return status;
	}

	bool channelList = argData.isFlagSet("-cl");
	bool flush = argData.isFlagSet("-f");

	if( (int)flush + (int)channelList != 1 )
	{
		MString msg = "OpenVDBCache: need to specify precisely one parameter";
		MGlobal::displayError(msg);
		status = MS::kFailure;
	}

	MStringArray objects;
	argData.getObjects(objects);

	if( flush )
	{
		/* Clear the cache. */
		t_VDBInfoCache::iterator it;
		for(it=GetVDBInfoCache().begin(); it!=GetVDBInfoCache().end(); it++)
		{
			delete it->second;
		}
		GetVDBInfoCache().clear();
	}
	else if(channelList && objects.length()>0)
	{
		MStringArray result;

		/* Resolve the filename */
		MString vdb_file = objects[0];
		if( vdb_file.length()>0 && objects.length()>1 )
		{
			/* Resolve the frame number */
			double frame = atof(objects[1].asChar());

			char expanded_text[2048];
			expandEnvVars(
				expanded_text,
				sizeof(expanded_text)/sizeof(expanded_text[0]),
				const_cast<char*>(vdb_file.asChar()),
				frame);
			vdb_file = expanded_text;

			vdb_file = Utilities::expandFToken( vdb_file, frame );
		}

		const OpenVDBVertices& vdb_stat = ReadCachedVertices(vdb_file.asChar() );

		/* Gat a name of all the grids of current VDB file */
		for( const auto &grid_name : vdb_stat.m_grids )
		{
			result.append( grid_name.c_str() );
		}

		setResult(result);
	}

	return status;
}

bool OpenVDBCache::GetGridVertices(
    const char* i_filename,
    const char* i_grid,
    const std::vector<float> **o_vertices,
    const std::vector<float> **o_colors)
{
	const OpenVDBVertices& vdb_stat = ReadCachedVertices(i_filename, i_grid);

	if(o_vertices)
	{
		*o_vertices = &(vdb_stat.m_vertices);
	}

	if(o_colors)
	{
		*o_colors = &(vdb_stat.m_colors);
	}

	return true;
}

