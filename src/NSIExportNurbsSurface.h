#ifndef __NSIExportNurbsSurface_h
#define __NSIExportNurbsSurface_h

#include "NSIExportDelegate.h"

#include <maya/MObject.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>

class NSIExportMesh;

/**
	\sa NSIExportDelegate
*/
class NSIExportNurbsSurface : public NSIExportDelegate
{
public:
	NSIExportNurbsSurface(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context );
	~NSIExportNurbsSurface();
	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );

private:
	MObject m_trs;
	MObject m_mesh;

	/* The mesh delegate */
	NSIExportMesh *m_delegate;
};

#endif
