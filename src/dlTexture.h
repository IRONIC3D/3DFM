#ifndef __dlTexture_H__
#define __dlTexture_H__

#include <maya/MPxNode.h>

class dlTexture : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
		static MObject s_textureFile;
		static MObject s_outColor;
		static MObject s_outAlpha;
};

#endif
