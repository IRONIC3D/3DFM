#ifndef __DL_STRINGOP_H__
#define __DL_STRINGOP_H__

#include <maya/MPxCommand.h>

class DL_stringOp : public MPxCommand
{

public:
  virtual MStatus       doIt(const MArgList& args);

  virtual bool          isUndoable() const
                        { return false; }

  static void*          creator()
                        { return new DL_stringOp; }

  static MSyntax        newSyntax();

private:
};

#endif
