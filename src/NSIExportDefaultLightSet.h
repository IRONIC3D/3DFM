#ifndef __NSIExportDefaultLightSet_h
#define __NSIExportDefaultLightSet_h

#include "NSIExportDelegate.h"
#include <maya/MNodeMessage.h>

/**
	\sa NSIExportDelegate
*/
class NSIExportDefaultLightSet : public NSIExportDelegate
{
public:
	NSIExportDefaultLightSet(
		MFnDependencyNode &,
		MObject &,
		const NSIExportDelegate::Context& i_context );

	virtual void Create();
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable *i_table );
	virtual bool RegisterCallbacks( void );

	static void attributeChangedCB(
		MNodeMessage::AttributeMessage i_msg,
		MPlug &i_plug, MPlug &i_otherPlug,
		void *i_data );
};
#endif
