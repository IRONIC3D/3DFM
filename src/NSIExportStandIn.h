#ifndef __NSIExportStandIn_h
#define __NSIExportStandIn_h

#include "NSIExportDelegate.h"

/// NSI Exporter for a 3Delight Stand-in node
class NSIExportStandIn : public NSIExportDelegate
{
public:

	/// Constructor
	NSIExportStandIn(
		MObject i_object,
		const NSIExportDelegate::Context& i_context);
	/// Destructor
	virtual ~NSIExportStandIn();

	/// Creates the NSI procedural node
	virtual void Create();
	/// Sets the NSI attributes of the node
	virtual void SetAttributes();
	/// Sets the node's zero time-dependent attribute.
	virtual void SetAttributesAtTime(double i_time, bool i_no_motion);
};

#endif
