#ifndef __DL_RIPRIMITIVENODE_H__
#define __DL_RIPRIMITIVENODE_H__

class DL_primVarList;

class DL_RiPrimitiveNode
{
public:
  DL_RiPrimitiveNode(bool supports_blur = 1)
  : m_supportsMotionBlur(supports_blur)
  {
    // nothing else to do
  }
  virtual ~DL_RiPrimitiveNode()
  {
    // nothing else to do
  }

  bool  supportsMotionBlur() const
        { return m_supportsMotionBlur; }

  /** Output interior shader, if exist */
  virtual void  emitRIBShader() {}
  virtual void  emitRIB() = 0;

  virtual unsigned numUniformValues() const { return 0; }
  virtual unsigned numVertexValues() const { return 0; }

  virtual DL_primVarList* getPrimVarList() { return 0; }

private:
  bool  m_supportsMotionBlur;
};

#endif
