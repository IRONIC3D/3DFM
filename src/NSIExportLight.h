#ifndef __NSIExportLight_h
#define __NSIExportLight_h

#include "NSIExportDelegate.h"
#include "NSIExportShader.h"

#include <maya/MStatus.h>
#include <maya/MNodeMessage.h>

#if MAYA_API_VERSION >= 201800
#include <maya/MApiNamespace.h>
#else
class MObject;
class MPlugArray;
#endif

/**
	\sa NSIExportDelegate
*/
class NSIExportLight : public NSIExportDelegate
{
	enum LightType
	{
		e_spot,
		e_distant,
		e_point,
		e_area,
		e_invalid
	};

public:
	NSIExportLight(
		MFnDagNode &i_dag,
		const NSIExportDelegate::Context& i_context );
	~NSIExportLight();
	virtual const char *NSINodeType( void ) const;
	virtual void Create( void );
	virtual void SetAttributes( void );
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable * );

	virtual bool RegisterCallbacks();

	void ConnectOneMeshLight( const MObject& i_mesh );
	void DisconnectOneMeshLight( const MObject& i_mesh );

	void ConnectDefaultMesh();
	void DisconnectDefaultMesh();
	void AdjustDefaultMeshConnection();

	static void AttributeChangedCallback( 
		MNodeMessage::AttributeMessage i_msg, 
		MPlug& i_plug, 
		MPlug& i_otherPlug, 
		void* i_data);

private:
	bool HasFilters( MPlugArray& o_filters ) const;
	void ConnectLightFilters();
	void FlushLightFilters();
	MStatus LightGeo( MObjectArray &o_meshes) const;
	MString DefaultMeshHandle() const;
	void CreateDefaultAreaLightShape( void );

private:
	LightType m_type;
	NSIExportShader m_shader_delegate;
	// The following are used by ConnectLightFilters and FlushLightFilters
	MString m_last_filter_handle;
	MString m_last_filter_param;
	unsigned m_num_mix_filters;

	/** = true if this is light is a 3Delight environment light */
	bool m_is_environment;
};

#endif
