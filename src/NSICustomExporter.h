/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __NSICustomExporter_h_
#define __NSICustomExporter_h_

#include "nsi.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MNodeMessage.h>
#include <maya/MObject.h>
#include <maya/MUuid.h>

/**
	A custom delegate plug-in is expected to define a Maya command named:

	<node_type_name>Exporter

	The command requires no flags and will produce a string	result; the
	result must be a pointer to a NSICustomExporter object allocated with
	new. The pointer should be produced as a string in a similar fashion
	to what sprintf(char*, "%p", ptr) would produce.

	The returned NSICustomExporter contains pointers to functions that
	3Delight for Maya will call when relevant to export nodes of a given type.
*/

/**
	Utility functions for NSI output.
*/
struct NSIUtils
{
public:
	NSIUtils() { };
	~NSIUtils() { };

	/**
		\brief Returns the NSI handle for a given Maya object.
	*/
	virtual MString NSIHandle( MObject i_node );

	/**
		\brief Returns the NSI handle of an eventual NSI attributes node related
		to the specified Maya object.
	*/
	virtual MString NSIAttributesHandle( MObject i_node );

	/**
		\brief Returns the NSI Handle of the shader node related to the specified
		Maya object.
	*/
	virtual MString NSIShaderHandle( MObject i_node );
};

struct NSICustomExporter
{
public:

	/**
		\brief Pointer to an exporter initialization function.

		This will be called only once per rendering, before any custom delegate
		is created.

		\param NSIContext_t
			The NSI context.

		\param i_nsiUtils
			A NSI utils object, valid for the duration of the InitializeFunc call.

		\param void*&
			Data pointer. This pointer will be passed to each delegate
			constructor, and to the exporter finalize function.
	*/
	typedef void (*InitializeFunc)( NSIContext_t, NSIUtils&, void*& );

	/**
		\brief Pointer to an exporter finalization function.

		This will be called only once per rendering, while the NSI context is
		still valid, and right before the custom delegates are destroyed.

		\param NSIContext_t
			The NSI context.

		\param i_nsiUtils
			A NSI utils object, valid for the duration of the Finalize call.

		\param void*
			Data pointer.
	*/
	typedef void (*FinalizeFunc)( NSIContext_t, NSIUtils&, void* );

	/**
		\brief Pointer to a NSICustomDelegate creation function.

		The function is expected to return a pointer to a NSICustomDelegate
		object allocated with new.

		\param MObject
			The Maya object to output as NSI statements.

		\param NSIContext_t
			The NSI context.

		\param const MString&
			The NSI handle 3Delight for Maya expects for the Maya object.

		\param i_nsiUtils
			A NSI utils object, valid during the existence of the delegate.

		\param void*
			Data pointer, as defined by the initialize function.
	*/
	typedef
		void* (*CreateDelegateFunc)(
			MObject, NSIContext_t, const MString&, NSIUtils&, void* );

	/**
		\brief default constructor.
	*/
	NSICustomExporter()
	:
	m_version( 2 ),
	m_padding( 0 ),
	m_initialize( 0x0 ),
	m_finalize( 0x0 ),
	m_createDelegate( 0x0 )
	{
	}

	/**
		\brief The NSICustomExporter struct version.
	*/
	const int m_version;

	/// Not used at this time.
	int m_padding;

	/**
		\brief The initialize function. Set to NULL if no initialization
		function is defined.
	*/
	InitializeFunc m_initialize;

	/**
		\brief The finalize function. Set to NULL if no finalization function is
		defined.
	*/
	FinalizeFunc m_finalize;

	/**
		\brief The NSICustomDelegate creation function.
	*/
	CreateDelegateFunc m_createDelegate;
};

/**
	\brief Details the various NSI statements required to properly output a
	given Maya object.
*/
class NSICustomDelegate
{
public:

	/**
		\brief Constructor.

		\param i_node
			The Maya Object to output to NSI.

		\param i_nsiContext
			The NSI context

		\param i_handle
			The NSI handle 3Delight for Maya expects the delegate to produce.

		\param i_nsiUtils
			A NSI utils object

		\param i_data
			Data pointer, as defined by the NSICustomExporter initialize function.
	*/
	NSICustomDelegate(
		MObject i_node,
		NSIContext_t i_nsiContext,
		const MString& i_handle,
		NSIUtils& i_nsiUtils,
		void* i_data )
	:
		m_object( i_node ),
		m_context( i_nsiContext ),
		m_handle( i_handle ),
		m_nsiUtils( i_nsiUtils )
	{
	}

	virtual ~NSICustomDelegate() { };

	/**
		\brief Creates the NSI node(s) required to render the Maya object.
	*/
	virtual void Create() { }

	/**
		\brief Set the NSI attributes of the nodes created during Create().

		This is called once per frame.
	*/
	virtual void SetAttributes() { }

	/**
		\brief Set the motion blurred NSI attributes of the nodes created during
		\ref Create().

		This is called once per motion sample.

		\param i_time
			The time at which to output the attributes

		\param i_no_motion
			There are is actually no motion blur in the render, the delegate
			can output attributes without relying on i_time
	*/
	virtual void SetAttributesAtTime( double i_time, bool i_no_motion ) { }

	/**
		\brief Connect the exporter's NSI nodes.

		This is called once all Create() and SetAttributes(...) functions have
		completed for all delegates.

		3Delight for Maya will handle the connection to a parent transform it
		outputed.
	*/
	virtual void Connect() { }

	/**
		\brief Processing that must be done after all \ref Connect() have
		completed on all delegates.
	*/
	virtual void Finalize() { }

	/**
		\brief Return true if this object is deformed.

		This will affect the interpretation of a deformation motion blur
		additional samples if one exists on the Maya object.
	*/
	virtual bool IsDeformed( ) const { return false; }

	/**
		\brief Register callbacks on Maya events to produce proper live render
		edits.

		When this function returns false, 3Delight for Maya will register some
		default callbacks and call the delegate's SetAttribute(*) and Connect()
		functions when these events occur.

		When this function returns true, 3Delight for Maya will not register
		any callbacks. The delegate is expected to registr and handle them
		itself. In this case, the delegate should unregister them in its
		destructor.
	*/
	virtual bool RegisterCallbacks() { return false; }

	/**
		\brief Use attributes from a DlSet to alter the behavior of the delegate.

		Only available when exporter version >= 2.
	*/
	virtual void OverrideAttributes( MObject& i_attributes ) { }

protected:
	MObject m_object;
	NSIContext_t m_context;
	MString m_handle;
	NSIUtils& m_nsiUtils;
};

#endif
