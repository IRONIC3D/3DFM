#ifndef __dlGlass_H__
#define __dlGlass_H__

#include <maya/MPxNode.h>

class dlGlass : public MPxNode
{
public:
	static void* creator();
	static MStatus initialize();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );

private:
	/*
		Not an actual shader parameter; allows the transparency VP2 shader fragment
		to work properly.
	*/
	static MObject s_transparency;

	static MObject s_color;
	static MObject s_outColor;
};

#endif

