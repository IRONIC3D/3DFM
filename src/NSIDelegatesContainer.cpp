
#include "NSIDelegatesContainer.h"
#include "NSIExportDelegate.h"

#include <maya/MObject.h>
#include <maya/MFn.h>

NSIDelegatesContainer::NSIDelegatesContainer()
{
}

/**
	\brief Delete all delegates.

	We delete in the inverse order of the itaratiom.
*/
NSIDelegatesContainer::~NSIDelegatesContainer()
{
	Clear();
}

/**
	\brief Delete all delegates and clear
*/
void NSIDelegatesContainer::Clear( void )
{
	m_dependency_nodes.clear();
	m_dag_nodes.clear();
}

void NSIDelegatesContainer::ResetIterator( void )
{
	m_dag_iterator = m_dag_nodes.begin();
	m_dependency_iterator = m_dependency_nodes.begin();
}

/**
	\brief Iterates through all the delegates.

	By requirements, we iterate first on the DAG nodes and then on the
	dependency nodes for correct scene output.
*/
NSIExportDelegate *NSIDelegatesContainer::Next( void )
{
	if( m_dag_iterator != m_dag_nodes.end() )
		return (m_dag_iterator++)->second.get();;

	if( m_dependency_iterator != m_dependency_nodes.end() )
		return (m_dependency_iterator++)->second.get();

	return nullptr;
}

void NSIDelegatesContainer::AddDelegate( NSIExportDelegate *i_delegate )
{
	if( i_delegate->Object().hasFn(MFn::kDagNode) )
		AddDagDelegate( i_delegate );
	else
		AddDependencyDelegate( i_delegate );
}

void NSIDelegatesContainer::AddDagDelegate( NSIExportDelegate *i_delegate )
{
	assert( i_delegate->Object().hasFn(MFn::kDagNode) );
	assert( m_dag_nodes.count(i_delegate->Handle()) == 0 );
	m_dag_nodes.insert_or_assign(
		i_delegate->Handle(), std::shared_ptr<NSIExportDelegate>(i_delegate));
}

void NSIDelegatesContainer::AddDependencyDelegate( NSIExportDelegate *i_delegate )
{
	m_dependency_nodes.insert_or_assign(
		i_delegate->Handle(), std::shared_ptr<NSIExportDelegate>(i_delegate));
}

/**
	\brief Return true if delegates contain a specific dag node.
	\param i_handle
		The NSI handle of the delegate to look for.
*/
bool NSIDelegatesContainer::ContainsDagNode( const char *i_handle ) const
{
	return m_dag_nodes.contains(i_handle);
}

/**
	\brief Delete the delegate using it's handle and all it's associtaed NSI
	nodes and all it's associtaed NSI nodes.
*/
bool NSIDelegatesContainer::Delete( const char *i_handle, bool i_recursive )
{
	auto it = m_dag_nodes.find( i_handle );
	if( it != m_dag_nodes.end() )
	{
		it->second->Delete( i_recursive );
		m_dag_nodes.erase(it);
		return true;
	}

	it = m_dependency_nodes.find( i_handle );
	if( it != m_dependency_nodes.end() )
	{
		it->second->Delete( i_recursive );
		m_dependency_nodes.erase(it);
		return true;
	}

	return false;
}

/**
	\brief Delete all DAG delegates of the given type
*/
void NSIDelegatesContainer::DeleteDagDelegates( MFn::Type i_type )
{
	for( auto it = m_dag_nodes.begin(); it != m_dag_nodes.end(); )
	{
		if( !it->second->Object().hasFn(i_type) )
		{
			++it;
			continue;
		}

		it->second->Delete();
		it = m_dag_nodes.erase(it);
	}
}


/**
	\brief Finds a particular delegate using its handle
*/
NSIExportDelegate *NSIDelegatesContainer::Find( const char *i_handle ) const
{
	auto it = m_dag_nodes.find(i_handle);
	if( it != m_dag_nodes.end() )
	{
		return it->second.get();
	}

	it = m_dependency_nodes.find(i_handle);
	if( it != m_dependency_nodes.end() )
	{
		return it->second.get();
	}

	return nullptr;
}
