#ifndef __dlCarPaint_H__
#define __dlCarPaint_H__

#include <maya/MPxNode.h>

class dlCarPaint : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_color;
	static MObject s_outColor;
};

#endif
