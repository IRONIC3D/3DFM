#include "dlGlass.h"

#include <maya/MFloatVector.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MObjectArray.h>

#include "DL_autoLoadOSL.h"
#include "DL_utils.h"
#include "OSLUtils.h"

#include <cassert>

MObject dlGlass::s_transparency;

MObject dlGlass::s_color;
MObject dlGlass::s_outColor;

void* dlGlass::creator()
{
	return new dlGlass();
}

MStatus dlGlass::initialize()
{
	MFnNumericAttribute numAttr;

	// hidden attribute; only exists to allow proper transparency shading.
	s_transparency = numAttr.createColor(
		"transparency",
		"transparency" );
	numAttr.setDefault( 0.9, 0.9, 0.9 );
	numAttr.setHidden( true );
	numAttr.setStorable( false );
	numAttr.setConnectable( false );
	addAttribute( s_transparency );

	// Other attributes are set based on the dlGlass OSL file.
	MStringArray shaderPaths = OSLUtils::GetBuiltInSearchPaths();
	MString shaderName( "dlGlass" );
	DlShaderInfo *info;

	OSLUtils::OpenShader( shaderName, shaderPaths, info );

	MObjectArray objects;
	MStringArray objectNames;

	DL_OSLShadingNode::CreateAttributesFromShaderParameters(
		info, 0x0, &objects, &objectNames );

	if(
		!DL_OSLShadingNode::FindAttribute(
			"color", s_color, objects, objectNames)
		|| !DL_OSLShadingNode::FindAttribute(
			"outColor", s_outColor, objects, objectNames) )
	{
		assert(false);
		// It makes no sense to return success here, but if we don't, Maya crashes.
		return MStatus::kSuccess;
	}

	for( unsigned i = 0; i < objects.length(); i++ )
	{
		addAttribute( objects[ i ] );
	}

	attributeAffects( s_color, s_outColor );

	MString name = info->shadername().c_str();
	MString niceName = OSLUtils::GetShaderNiceName( info );
	DL_OSLShadingNode::DefineShaderNiceName( name, niceName );

	return MStatus::kSuccess;
}

void dlGlass::postConstructor()
{
	MFnDependencyNode dep_node(thisMObject());

	/* Set the default node name */
	MString defaultname = typeName() + "#";

	/* Save the digit in the node name */
	if( isdigit( defaultname.asChar()[0] ) )
	{
		defaultname = "_" + defaultname;
	}

	dep_node.setName(defaultname);

	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

MStatus dlGlass::compute(
    const MPlug& i_plug,
    MDataBlock& i_block )
{
	if ((i_plug != s_outColor) && (i_plug.parent() != s_outColor))
	{
		return MS::kUnknownParameter;
	}

	/* Just transfer s_color to s_outColor */
	MFloatVector& color = i_block.inputValue( s_color ).asFloatVector();

	/* set ouput color attribute */
  	MDataHandle outColorHandle = i_block.outputValue( s_outColor );
	MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;
	outColorHandle.setClean();

	return MS::kSuccess;
}
