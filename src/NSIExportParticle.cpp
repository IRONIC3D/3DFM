#include "NSIExportParticle.h"
#include "DL_utils.h"
#include "dlVec.h"

#include <nsi.hpp>

#include <maya/MGlobal.h>
#include <maya/MFnTransform.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h>
#include <maya/MVectorArray.h>
#include <maya/MPointArray.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>
#include <maya/MMatrix.h>

#include <vector>

#include <assert.h>

void NSIExportParticle::Create()
{
	/* FIXME: could be curves too */
	NSICreate( m_nsi, m_nsi_handle, "particles", 0, 0x0 );
}

/**
	All transform attributes are time-dependent.
*/
void NSIExportParticle::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kParticle) );

	return;
}

/**
	Code and comments from DL_particleSystemSample.cpp

	We used the deformed geo for position and velocities

	FIXME: don't export particle ID when no motion blur ?
*/
void NSIExportParticle::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	assert( IsValid() );

	MStatus status;
	MFnParticleSystem particle_fn(Object(), &status);
	if (status != MStatus::kSuccess)
	{
		assert( false );
		return;
	}

	/*
		We can't rely on the deformed particle shape to contain valid attr
		values for anything not related to position. So we need to get the
		undeformed node provided by the particle system.

		NOTE: Must pass status to isDeformedParticleShape below or Maya crashes
	*/
	MObject undeformed_node = particle_fn.isDeformedParticleShape(&status) ?
		particle_fn.originalParticleShape(&status) : Object();

	unsigned num_particles = getAttrInt(undeformed_node, "count", 0);
	/*
		Get the ids of the particles that exist in this sample.
	*/
	MDoubleArray ids;
	bool has_ids = getAttrDoubleArray( undeformed_node, "particleId", ids);

	if( !has_ids || ids.length() != num_particles)
	{
		MString error;
		error += "3Delight for Maya: can't render particle system " +
		error += Handle();
		error += " because .particleId' attribute is not accessible.";
		MGlobal::displayError(error);
		return;
	}

	// Convert IDs format from double to int
	std::vector<int> int_ids;
	for(unsigned p = 0; p < num_particles; p++)
	{
		double did = ids[p];
		int iid = int(did);

		/*
			Check that the double actually contained an integer. If this stops
			to be the case, then we'll have to hash them into an integer.
		*/
		assert(double(iid) == did);

		int_ids.push_back(iid);
	}

	/* Will contain particle's positions */
	std::vector<dl::V3f> P;
	P.reserve( num_particles );

	if( undeformed_node.hasFn(MFn::kNCloth) )
	{
		/*
			We should be able to retrieve correct positions simply by
			interrogating the positions attribute, but for some reason this
			returns the positions of the nCloth particles where they were at
			the last frame. The only way to get proper positions as of Maya
			2008 seems to get the vertices of the connected output mesh.
		*/
		MFnDependencyNode undeformed_dep_node;
		undeformed_dep_node.setObject( undeformed_node );

		MPlug output_mesh_plug = undeformed_dep_node.findPlug("outputMesh", true);
		MPlugArray plugs_connected_to_output_mesh;
		output_mesh_plug.connectedTo(
			plugs_connected_to_output_mesh, false, true, &status );

		MDagPath mesh_path;
		status = MDagPath::getAPathTo(
			plugs_connected_to_output_mesh[0].node(), mesh_path );

		MFnMesh output_mesh( mesh_path, &status );
		if( status != MStatus::kSuccess )
		{
			MString error = "3Delight for Maya: can't render particle system ";
			error += Handle();
			error += " because can't access particles position data.";
			return;
		}

		MPointArray vertex_array;
		output_mesh.getPoints(vertex_array, MSpace::kObject);
		for( int i=0; i<vertex_array.length(); i++)
		{
			const MPoint &p = vertex_array[i];
			P.push_back( dl::V3f(p[0],p[1],p[2]) );
		}
	}
	else
	{
		MVectorArray position_array;
		getAttrVectorArray(Object(), "position", position_array);

		for( int i = 0; i<position_array.length(); i++ )
		{
			const MVector &p = position_array[i];
			P.push_back( dl::V3f(p[0],p[1],p[2]) );
		}
	}

	/*
		Get the particle's type. Note that both sphere and point are rendered
		using the same NSI 'sphere' type. We do this because a flat particle
		would beed a normal and Maya doesn't have this concept.

		3=points, 4=spheres, 5=sprites, 6=streaks, 7=blobby, 8=cloud
	*/
	int particle_type = getAttrInt( undeformed_node, "particleRenderType", 3 );

	if( particle_type!=3 || particle_type !=4 )
	{
		MString error = "3Delight for Maya: unsupported particle type in ";
		error += Handle();
		error += " will be rendered as a point.";
	}

	MDoubleArray radius_array;
	getAttrDoubleArray( undeformed_node, "radiusPP", radius_array );

	std::vector<float> widths;
	widths.reserve( radius_array.length() );

	for( int i=0; i<radius_array.length(); i++ )
		widths.push_back( radius_array[i] * 2.0f );

	/*
		If no per-particle radii, use a constant width (radius of 0.5 seems to
		be the default)
	*/
	if( widths.empty() )
	{
		double radius = getAttrDouble( undeformed_node, "radius", 0.5f );
		widths.push_back( 2.0f * float(radius) );
	}

	NSI::Context nsi(m_nsi);

	NSI::ArgumentList args;
	args.Add( NSI::Argument::New( "P" )
		->SetType(NSITypePoint)
		->SetCount(P.size())
		->SetValuePointer(&P[0]) );

	args.Add( NSI::Argument::New( "width" )
		->SetType(NSITypeFloat)
		->SetCount(widths.size())
		->SetValuePointer( &widths[0] ) );

	args.Add( NSI::Argument::New( "id" )
		->SetType(NSITypeInteger)
		->SetCount(int_ids.size())
		->SetValuePointer( &int_ids[0] ) );

	if( i_no_motion )
		nsi.SetAttribute( Handle(), args );
	else
		nsi.SetAttributeAtTime( Handle(), i_time, args );
}

/**
	\brief Particles are alwayas deformed.
*/
bool NSIExportParticle::IsDeformed( void ) const
{
	return true;
}
