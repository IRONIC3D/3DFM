#ifndef __DL_ABOUT_H__
#define __DL_ABOUT_H__

#include <maya/MPxCommand.h>

class DL_about : public MPxCommand
{
public:
  virtual MStatus doIt(const MArgList& args);

  virtual bool isUndoable() const { return false; }

  static void* creator() { return new DL_about; }

  static MSyntax newSyntax();
private:
};

#endif
