/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __VPUtilities__
#define __VPUtilities__

#include <maya/MObject.h>
#include <maya/MString.h>
#include <maya/MStringArray.h>
#include <maya/MDagPath.h>

/**
	Utility functions related to the Maya viewport shading fragments.
*/
namespace VPUtilities
{
	/**
		\brief Returns the path for user-defined fragment files.

		An empty string is returned if none is defined or when the path does not
		exist.
	*/
	MString GetUserFragmentPath();

	/**
		\brief Returns the path for built-in fragment files, i.e. the fragment files
		provided with 3Delight.
	*/
	MString GetBuiltinFragmentPath();

	/**
		\brief Add a shader fragment search path to the fragment manager.

		\param i_name
			The search path to add.
	*/
	void AddFragmentPath( const MString& i_path );

	/**
		\brief Add the user & built-in paths to the fragment manager.
	*/
	void AddFragmentPaths();

	/**
		\brief Add a single fragment.

		\param i_name
			The fragment name to add. A corresponding i_name.xml file will be
			searched for.

		\param i_isGraph
			Specifies if the fragment must be added as a fragment graph or a
			regular fragment.

		\return The registered fragment name.
	*/
	MString AddFragment( const MString& i_name, bool i_isGraph );

	/**
		\brief Remove a single fragment.

		\param i_name
			The fragment name to remove.

		\return True if the fragment was removed
	*/
	bool RemoveFragment( const MString& i_name );

	/**
		\brief Returns true if the specified fragment exists in the fragment
		manager.

		\param i_name
			The fragment name to remove.

		\return True if the fragment was found
	*/
	bool FragmentExists( const MString& i_name );

	/**
		\brief Clears the Maya shader manager effect cache.

		This is one of the required steps for the viewport to reflect changes after
		shader fragments have been removed (the other one being the unregistering
		of the related viewport shader override)
	*/
	void ClearShaderMgrEffectCache();

	/**
		\brief Returns the fragment name for the specified shading node type.

		An empty string is returned when no fragment was found.
	*/
	MString GetFragmentName( const MString& i_nodeTypeName );

	/**
		\brief Returns a list of fragment names kown by Maya's fragment manager that are
		not hidden.
	*/
	void GetFragmentList( MStringArray& o_list );

	/**
		\brief Returns the XML content of the specified fragment name.

		\param i_fragmentNamne
			The name of the fragment to inspect.

		\param o_buffer
			The MString that will be set to the XML content of the fragment.
	*/

	MStatus GetFragmentContent(
		const MString& i_fragmentName,
		MString& o_buffer );

	/**
		\brief Returns the XML content of the fragment for a specific shading node.

		\param i_shadingNode
			The shading node whose fragment is desired.

		\param o_buffer
			The MString that will be set to the XML content of the fragment.

		\param i_includeUpstreamNodes
			As per MFragmentManager's class doc:
			return the XML code of the fragment graph that Maya would use to represent
			the shading node network rooted at the given shading node in Viewport 2.0.

		\param i_objContext
			A pointer to a MDagPath that may be used by the fragment manager for nodes
			that depend it (such as the single / double / tripe switch nodes ).
			Can be set to NULL.
	*/
	MStatus GetFragmentContent(
		const MObject& i_shadingNode,
		MString& o_buffer,
		bool i_includeUpstreamNodes,
		MDagPath* i_objContext );

	/**
		\brief Set the path to use for dumping final effect files.

		This is a wrapper around MFragmentManager::setEffectOutputDirectory.
	*/
	void SetEffectOutputDir(const MString& i_dir);

	/**
		\brief
			Set the path to use for dumping intermediate fragment graph xml files.

		This is a wrapper around
		MFragmentManager::setIntermediateGraphOutputDirectory.
	*/
	void SetGraphOutputDir(const MString& i_dir);

	/**
		\brief Returns the classification string related to the VP shader
		override of regular shading nodes.
	*/
	MString GetVPShaderOverrideClass();

	/**
		\brief Returns the classification string related to the VP shader
		override of surface shading nodes.
	*/
	MString GetVPSurfaceShaderOverrideClass();

	/**
		\brief Registers the class derived from MPxShadingNodeOverride we use
		for regular shading nodes.
	*/
	void RegisterVPShaderOverride();

	/**
		\brief Deregisters the class derived from MPxShadingNodeOverride we use
		for regular shading nodes.
	*/
	void DeregisterVPShaderOverride();

	/**
		\brief Registers the class derived from  MPxSurfaceShadingNodeOverride
		we use for surface shading nodes.
	*/
	void RegisterVPSurfaceShaderOverride();

	/**
		\brief Registers the class derived from  MPxSurfaceShadingNodeOverride
		we use for surface shading nodes.
	*/
	void DeregisterVPSurfaceShaderOverride();
}

#endif
