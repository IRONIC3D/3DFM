#ifndef __NSIExportIncandescenceLight_h
#define __NSIExportIncandescenceLight_h

#include <maya/MFnSet.h>
#include <maya/MTypes.h>
#include <vector>

#if MAYA_API_VERSION >= 201500

#include "NSIExportDelegate.h"

/**
	\brief Export for our Incandescent Lights

	This is a weird one as it acts on the materials assigned to the geometries
	specifies in this light (through the _3delight_geometry attribute).

	Caveats:
	- Illuminate by Default + Light Linking doesn't work yet
	- If a meterial is shared between many objects, even objects not controlled
	  by this light will see their incandescence affected. This is because we
	  set attributes on object's materials.
*/
class NSIExportIncandescenceLight : public NSIExportDelegate
{
public:
	NSIExportIncandescenceLight(
		MFnDagNode &,
		const NSIExportDelegate::Context& i_context );
	virtual ~NSIExportIncandescenceLight();

	virtual const char* NSINodeType() const;
	virtual void Create();
	virtual void SetAttributes();
	virtual void SetAttributesAtTime( double, bool );
	virtual void Connect( const DelegateTable *i_dag_hash );

private:
	typedef void (NSIExportIncandescenceLight::*Fct)(
		const MDagPath&, const DelegateTable *, float []);
	void ScanAttrGeometry(Fct i_fct, const DelegateTable *dag, float i_color[]);
	void ComputeOutputColor(float o_color[]);

	void SetAttributeForShader(
		const MDagPath& i_dagPath, const DelegateTable *, float i_color[]);

	/** \brief Connects controlled objects to m_nsi_set. */
	void ConnectToObjects(
		const MDagPath& i_dagPath, const DelegateTable *, float i_color[]);

	bool GetShader(
		const char* i_type,
		const MFnSet& i_set,
		MObject& o_shader) const;

	void SetIncandescence(
		const MFnDagNode& i_dagNode,
		const MFnDependencyNode& i_shader,
		float i_color[] );

private:
	/** This will have the same name as the parent transform. */
	MString m_set_handle;

	/**
		We have to keep track of the currently "managed" materials as we have
		to reset their multipliers to 1 during live operation. When
		SetAttributes is called the _3delight_geometry attribute already
		contains the new value and we need to access the old state.

		\see SetAttributes
	*/
	std::vector< MString > m_current_multipliers;
};

#endif

#endif
