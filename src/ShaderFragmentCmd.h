#ifndef __ShaderFragmentCmd_h__
#define __ShaderFragmentCmd_h__

#include <maya/MPxCommand.h>

#define SHADERFRAGMENTCMD_STR "delightShaderFragment"

class ShaderFragmentCmd : public MPxCommand
{
public:
	virtual MStatus doIt( const MArgList& i_argList );

	virtual bool isUndoable() const { return false; }

	static void* creator() { return new ShaderFragmentCmd; }

	static MSyntax newSyntax();
};

#endif
