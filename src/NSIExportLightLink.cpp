#include "NSIExportLightLink.h"

#include "DelegateTable.h"
#include "NSIExportPriorities.h"
#include <nsi.hpp>

#include <maya/MNodeMessage.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>

#include <assert.h>

NSIExportLightLink::NSIExportLightLink(
		MFnDependencyNode &dep,
		MObject &obj,
		const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( dep, obj, i_context ),
	m_light_node(), m_object_node()
{
	assert( m_light_node.isNull() );
	assert( m_object_node.isNull() );
}

void NSIExportLightLink::Create()
{
	NSICreate( m_nsi, m_nsi_handle, "transform", 0, 0x0 );
}

void NSIExportLightLink::SetAttributes( void )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kLightLink) );

	return;
}

void NSIExportLightLink::SetAttributesAtTime(
	double i_time, bool i_no_motion )
{
	assert( IsValid() );
	assert( Object().hasFn(MFn::kLightLink) );

	return;
}

void NSIExportLightLink::Connect( const DelegateTable *i_dag_hash )
{
	MFnDependencyNode light_linker( Object() );

	MPlug link = light_linker.findPlug( "link", true );
	MPlug ignore = light_linker.findPlug( "ignore", true );

	assert( link.isArray() );
	assert( ignore.isArray() );

#if 0
	MPlugArray plugs;
	light_linker.getConnections( plugs );

	for( int i=0; i<plugs.length(); i++ )
	{
		fprintf( stderr, "%d %s\n", i, plugs[i].partialName().asChar() );
	}
#endif

	DoInterObjectVisibility( i_dag_hash, link, true );
	DoInterObjectVisibility( i_dag_hash, ignore, false );
}

/**
	\brief Dictates to NSI how an object is visible from a light source.

	\param i_link
		A compound plug containing two arrays of the same size linking a light
		source to an object.

	\param i_priority
		NSI's visibility priority
*/
void NSIExportLightLink::DoInterObjectVisibility(
	const DelegateTable *i_dag_hash,
	const MPlug &i_link, bool i_visibility )
{
	NSI::Context nsi( m_nsi );
	//i_link.evaluateNumElements(); // not sure about this.

	for(int i = 0; i < i_link.numElements(); i++)
	{
		const MPlug &one_link = i_link[i];

		assert( one_link.isCompound() );
		assert( one_link.numChildren() == 2 );

		MPlug light_plug = one_link.child(0);
		MPlug object_plug = one_link.child(1);

		MPlugArray light, object;
		light_plug.connectedTo(light, true, false);
		object_plug.connectedTo(object, true, false);

		if( light.length()==0 || object.length()==0 )
			continue; // happens!

		bool light_is_set = light[0].node().hasFn(MFn::kSet);
		bool object_is_set = object[0].node().hasFn(MFn::kSet);

		MString light_handle =
			NSIExportDelegate::NSIHandle(light[0].node(), m_live);
		MString object_handle =
			NSIExportDelegate::NSIHandle(object[0].node(), m_live);

		MString light_attributes =
			NSIExportDelegate::NSIAttributesHandle(light[0].node(), m_live);

		MString object_attributes =
			NSIExportDelegate::NSIAttributesHandle(object[0].node(), m_live);

		/*
			FIXME:
			Shading engine nodes are light-linkable and are currently output as
			attributes node.

			Default light set is output as an attributes nodes while all other
			sets are output as NSI Set.
		*/
		if( object[0].node().hasFn( MFn::kShadingEngine ) )
		{
			/*
				Shading engine nodes are light-linkable and are currently
				output as attributes node.
			*/
			object_attributes = object_handle;
		}
		if( light_is_set &&
			MFnDependencyNode( light[0].node() ).name() == "defaultLightSet" )
		{
			light_attributes = light_handle;
		}

		if( i_dag_hash )
		{
			/*
				Don't look for non-DAG objects in i_dag_hash - they won't be
				found and we can't infer anything from that search result.
			*/
			bool light_is_dag = light[0].node().hasFn(MFn::kDagNode);
			bool obj_is_dag = object[0].node().hasFn(MFn::kDagNode);

			if( (light_is_dag && !i_dag_hash->contains(light_handle)) ||
			    (obj_is_dag && !i_dag_hash->contains(object_handle)) )
			{
				continue;
			}
		}

		int priority = GetPriority( light_is_set, object_is_set );

		/*
			Create a visibility attributes here in case there are no attributes
			associated with the object yet. if object is already a set it is
			an attribute itself. FIXME: should be an NSI set really.
		*/
		if( !object_is_set )
		{
			nsi.Create( object_attributes.asChar(), "attributes" );
			nsi.Connect(
				object_attributes.asChar(), "",
				NSIExportDelegate::NSIHandle(object[0].node(), m_live).asChar(),
				"geometryattributes" );
		}

#if 0
		fprintf( stderr, "connecting %s to %s with %d\n",
			object_attributes.asChar(),
			light_attributes.asChar(), priority );
#endif

		nsi.Connect(
			object_attributes.asChar(), "",
			light_attributes.asChar(), "visibility",
			(
				NSI::IntegerArg( "value", i_visibility ? 1 : 0),
				NSI::IntegerArg( "priority", priority)
			) );
	}
}

/**
	\brief Link/unlink like/object pairs.

	Note that we wait to have a complete pair here. We need a small state
	in the delegate for that.

	We use the plug's name to know if it's a light or an ddobject we are
	receiving in "otherPlug". Using the MFn type seems risky the plug could
	potentially point to sets/transforms.
*/
void NSIExportLightLink::static_attributeChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug, MPlug &i_otherPlug,
	void *i_data )
{
	NSIExportLightLink *delegate = (NSIExportLightLink *)i_data;
	delegate->attributeChangedCB( i_msg, i_plug, i_otherPlug );
}

void NSIExportLightLink::attributeChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug &i_plug, MPlug &i_otherPlug )
{
	if( !(i_msg &
		(MNodeMessage::kConnectionMade | MNodeMessage::kConnectionBroken)) )
	{
		return;
	}

	bool is_light = i_plug.name().indexW(".light")!=-1;
	if( is_light )
	{
		assert( m_light_node.isNull() );
		m_light_node = i_otherPlug.node();
	}
	else if( i_plug.name().indexW(".object")!=-1 )
	{
		assert( m_object_node.isNull() );
		m_object_node = i_otherPlug.node();
	}

	bool edit_completed = !m_object_node.isNull() && !m_light_node.isNull();
	if( !edit_completed )
		return;

	bool light_is_set = m_light_node.hasFn(MFn::kSet);
	bool object_is_set = m_object_node.hasFn(MFn::kSet);

	MString object_attributes =
		NSIExportDelegate::NSIAttributesHandle(m_object_node, m_live);

	MString light_attributes =
		NSIExportDelegate::NSIAttributesHandle(m_light_node, m_live);

	/*
		FIXME:
		Shading engine nodes are light-linkable and are currently output as
		attributes node.

		Default light set is output as an attributes nodes while all other sets
		are output as NSI Set.
	*/
	if( m_object_node.hasFn( MFn::kShadingEngine ) )
	{
		object_attributes = NSIExportDelegate::NSIHandle(m_object_node, m_live);
	}

	if( light_is_set &&
		MFnDependencyNode( m_light_node ).name() == "defaultLightSet" )
	{
		light_attributes = NSIExportDelegate::NSIHandle(m_light_node, m_live);
	}

	NSI::Context nsi(m_nsi);

	/*
		Make sure object attributes are created for the node. We could have
		started in a state where they are not (this is no just a nicety,
		this happens all the time because initial links might not be present
		at SetAttributes() time)
	*/
	if( !object_is_set )
	{
		nsi.Create( object_attributes.asChar(), "attributes" );
		nsi.Connect( object_attributes.asChar(), "",
			NSIExportDelegate::NSIHandle(m_object_node, m_live).asChar(),
			"geometryattributes" );
	}

	/* robustness reset for asserts in next edit */
	m_light_node = m_object_node = MObject::kNullObj;

	if( i_msg & MNodeMessage::kConnectionBroken )
	{
		nsi.Disconnect(
			object_attributes.asChar(), "",
			light_attributes.asChar(), "visibility" );
	}
	else
	{
		assert( i_msg & MNodeMessage::kConnectionMade );
		int visibility = i_plug.name().indexW(".ignore")!=-1 ? 0 : 1;
		int priority = GetPriority( light_is_set, object_is_set );
		nsi.Connect(
			object_attributes.asChar(), "",
			light_attributes.asChar(), "visibility",
			(
				NSI::IntegerArg("value", visibility),
				NSI::IntegerArg("priority", priority)
			) );
	}

	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}


/**
	\brief Register a callback to monitor changes in set memebership
*/
bool NSIExportLightLink::RegisterCallbacks( void )
{
	MObject &object = (MObject &)Object();
	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		object, static_attributeChangedCB, (void *)this );
	AddCallbackId( id );

	return true;
}

/**
	\brief Returns the priority of the link, knowing it's end points.

	shape to shape are the highest priority. Anything coming from an object set
	(Shading group) is second highest. Last is something is connected to a
	light set (default light set). All still override light sources base
	priority (it is created invisible).
*/
int NSIExportLightLink::GetPriority( bool light_is_set, bool object_is_set )
{
	int priority;
	if( light_is_set )
		priority = NSI_LIGHTSET_PRIORITY;
	else if( object_is_set )
		priority = NSI_OBJECTSET_PRIORTY;
	else
		priority = NSI_LIGHTLINK_PRIORITY;

	return priority;
}

