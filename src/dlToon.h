#ifndef __dlToon_H__
#define __dlToon_H__

#include <maya/MPxNode.h>

class dlToon : public MPxNode
{
public:
  static void* creator();
  static MStatus initialize();

  virtual void postConstructor();
  virtual MStatus compute( const MPlug& i_plug, MDataBlock& i_dataBlock);

private:
	static MObject s_color_tint;
	static MObject s_outColor;
};

#endif
