/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#include "DL_fileSequence.h"

#include <maya/MAnimControl.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MPlug.h>
#include <maya/MPxNode.h>

#include <math.h>

#include "DL_utils.h"

MStatus
DL_fileSequence::initialize()
{
	using namespace Utilities;

	MStatus status;
	MFnNumericAttribute numAttr;

	m_fileSequenceMode = numAttr.create(
		"fileSequenceMode", "fsMode", MFnNumericData::kInt, 0, &status);
	makeInputAttribute( numAttr );

	m_firstIndex = numAttr.create(
		"fileSequenceFirstIndex", "fsFirst", MFnNumericData::kInt, 1, &status);
	makeInputAttribute( numAttr );

	m_lastIndex = numAttr.create(
		"fileSequenceLastIndex", "fsLast", MFnNumericData::kInt, 1, &status);
	makeInputAttribute( numAttr );

	m_offset = numAttr.create(
		"fileSequenceOffset", "fsOffset", MFnNumericData::kInt, 0, &status);
	makeInputAttribute( numAttr );

	m_increment = numAttr.create(
		"fileSequenceIncrement", "fsIncrement", MFnNumericData::kInt, 1, &status);
	numAttr.setMin(0);
	makeInputAttribute( numAttr );

	m_outsideRangeMode = numAttr.create(
		"fileSequenceOutsideRangeMode", "fsorMode", MFnNumericData::kInt, 0, &status);
	makeInputAttribute( numAttr );

	m_frameRangeMode = numAttr.create(
		"fileSequenceFrameRangeMode", "fsfrMode", MFnNumericData::kInt, 0, &status);
	makeInputAttribute( numAttr );

	m_frameRangeStart = numAttr.create(
		"fileSequenceFrameRangeStart", "fsStart", MFnNumericData::kInt, 1, &status);
	makeInputAttribute( numAttr );

	m_frameRangeEnd = numAttr.create(
		"fileSequenceFrameRangeEnd", "fsEnd", MFnNumericData::kInt, 1, &status);
	makeInputAttribute( numAttr );

  MPxNode::addAttribute(m_fileSequenceMode);
  MPxNode::addAttribute(m_firstIndex);
  MPxNode::addAttribute(m_lastIndex);
  MPxNode::addAttribute(m_offset);
  MPxNode::addAttribute(m_increment);
  MPxNode::addAttribute(m_outsideRangeMode);
  MPxNode::addAttribute(m_frameRangeMode);
  MPxNode::addAttribute(m_frameRangeStart);
  MPxNode::addAttribute(m_frameRangeEnd);

	return status;
}

int
DL_fileSequence::getSequenceNumber(MObject i_obj, double i_currentFrame)
{
	int frame = floor(i_currentFrame + 0.5);

	// fileSequenceMode
	// 0 = off - use current frame number
	// 1 = use the various range attributes
	//
	MPlug modePlug(i_obj, m_fileSequenceMode);
	int mode = 0;
	modePlug.getValue(mode);

	if(mode == 0)
	{
		return frame;
	}

	MPlug firstIndexPlug(i_obj, m_firstIndex);
	int firstIndex = 1;
	firstIndexPlug.getValue(firstIndex);

	MPlug lastIndexPlug(i_obj, m_lastIndex);
	int lastIndex = 0;
	lastIndexPlug.getValue(lastIndex);

	MPlug incrementPlug(i_obj, m_increment);
	int increment = 1;
	incrementPlug.getValue(increment);

	// outsideRangeMode
	// 0 = hold ( clamp(start, end) )
	// 1 = loop
	//
	MPlug outsideRangeModePlug(i_obj, m_outsideRangeMode);
	int outsideRangeMode = 0;
	outsideRangeModePlug.getValue(outsideRangeMode);

	int offset = 0;
	MPlug offsetPlug(i_obj, m_offset);
	offsetPlug.getValue(offset);

	if(firstIndex > lastIndex)
	{
		increment = -increment;
		offset = -offset;
	}

	// frameRangeMode
	// 0 = off - apply on whole scene
	// 1 = use sequence inside defined range; hold first / last file outside.
	//
	MPlug frameRangeModePlug(i_obj, m_frameRangeMode);
	int frMode = 0;
	frameRangeModePlug.getValue(frMode);

	int frStart = 0;
	int frEnd = 0;

	if(frMode > 0)
	{
		MPlug frStartPlug(i_obj, m_frameRangeStart);
		frStartPlug.getValue(frStart);

		MPlug frEndPlug(i_obj, m_frameRangeEnd);
		frEndPlug.getValue(frEnd);
	}
	else
	{
		// Use the scene's animation in / out
		frStart = MAnimControl::animationStartTime().value();
		frEnd = MAnimControl::animationEndTime().value();
	}

	if(frame < frStart)
	{
		frame = frStart;
	}
	else if(frame > frEnd)
	{
		frame = frEnd;
	}

	int normalized_frame = frame - frStart;

	int fileIndex = offset + normalized_frame * increment;

	int minIndex = firstIndex < lastIndex ? firstIndex : lastIndex;
	int maxIndex = firstIndex > lastIndex ? firstIndex : lastIndex;

	if(outsideRangeMode == 0)
	{
		fileIndex += firstIndex;

		// clamp to range
		if ( fileIndex < minIndex)
			fileIndex = minIndex;
		else if (fileIndex > maxIndex)
			fileIndex = maxIndex;
	}
	else
	{
		// loop
		int sequenceLength = maxIndex - minIndex + 1;

		if( fileIndex < 0 )
		{
			/*
				When handling a negative fileIndex (possible when offset is negative),
				substract the remainder of the division by the sequence length from the
				end of the sequence.
			*/
			fileIndex = abs(fileIndex) % sequenceLength;
			fileIndex = lastIndex - fileIndex;
		}
		else
		{
			fileIndex %= sequenceLength;
			fileIndex += firstIndex;
		}
	}
	return fileIndex;
}
