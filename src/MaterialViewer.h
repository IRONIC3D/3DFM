/*
  Copyright (c) The 3Delight Team.
  Copyright (c) soho vfx inc.
*/

#ifndef __DL_RENDERER_H
#define __DL_RENDERER_H

#include <maya/MGlobal.h>

#include <vector>

#if MAYA_API_VERSION >= 201600
#include <maya/MPxRenderer.h>
#include <maya/MThreadAsync.h>
#include <maya/MImage.h>
#include <maya/MObjectHandle.h>

#include "RenderPassInterface.h"

#include "nsi.h"

class NSIExportCamera;

/**
	Implement a "mini" render pass that we can pass to NSIExportCamera
	\sa NSIExportCamera
*/
class MiniRenderPass : public RenderPassInterfaceForCamera,
	public RenderPassInterfaceForScreen
{
public:
	MiniRenderPass( int xres, int yres)
	:
		m_xres(xres), m_yres(yres)
	{
	}

	MiniRenderPass()
	:
		m_xres(0), m_yres(0)
	{
	}

	/* ForScreen */
	int ResolutionX( void ) const { return m_xres; }
	int ResolutionY( void ) const { return m_yres; }
	double PixelAspectRatio( void ) const { return 1.0; }
	void Crop(float o_top_left[2], float o_bottom_right[2]) const
	{
		o_top_left[0] = 0.0f; o_top_left[1] = 0.0;
		o_bottom_right[0] = 1.0f; o_bottom_right[1] = 1.0f;
	}
	int PixelSamples() const { return 16; }
	int PixelFilterIndex() const { return 0; }

	const char *PixelFilter( void ) const { return "blackman-harris"; }
	double FilterWidth( void ) const { return 2.0; }
	bool DisableMotionBlur() const { return false; }
	bool DisableDepthOfField() const { return false; }

private:
	int m_xres, m_yres;
};

class MaterialViewer : public MPxRenderer
{
public:
	MaterialViewer();
	virtual ~MaterialViewer();

	static void* creator();

	// @{
	/** Implementation of the MPxRenderer interface. */
	MStatus startAsync(const JobParams& i_params);
	MStatus stopAsync();
	bool isRunningAsync();
	MStatus beginSceneUpdate();
	virtual MStatus endSceneUpdate();
	MStatus translateMesh(const MUuid& , const MObject& );
	MStatus translateLightSource( const MUuid& , const MObject& );
	MStatus translateCamera( const MUuid& i_id, const MObject& );
	MStatus translateEnvironment( const MUuid&, EnvironmentType);
	MStatus translateTransform( const MUuid&, const MUuid&, const MMatrix&);
	MStatus translateShader(const MUuid&,const MObject&);
	MStatus setProperty(const MUuid&, const MString&, bool);
	MStatus setProperty( const MUuid&,const MString&, int);
	virtual MStatus setProperty(const MUuid&, const MString&, float i_value);
	virtual MStatus setProperty( const MUuid&, const MString&, const MString&);
	virtual MStatus setShader(const MUuid&, const MUuid&);
	virtual MStatus setResolution(unsigned int i_width, unsigned int i_height);
	virtual MStatus destroyScene();
	virtual bool isSafeToUnload();
	// @}

public:
	// Pass a rendered bucket from the display driver to Maya
	void receiveBucket(int i_xmin, int i_xmax_plus_one,
		int i_ymin, int i_ymax_plus_one,
		int i_entrySize,
		void* i_data);

	// Define the number of times we should expect to receive a entire
	// rendered images before we can consider it complete
	void setNumPasses(int i_numPasses);

private:
	/**
		\brief Outputs driver and layer needed for this render.
		\param i_camera_handle
			The NSI handle for the rendering camera.
	*/
	void OutputDisplayChain( const char *i_camera_handle ) const;

private:
	NSIContext_t m_nsi;

	bool m_started;
	bool m_camera_alread_output;

	int m_numReceivedPixels;
	int m_numExpectedPixels;
};


#endif
#endif
