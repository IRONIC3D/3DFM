#include "DL_packageSpecific.h"

#include "DL_nsiArchiveTranslator.h"

#define MNoPluginEntry
#define MNoVersionString
#include <maya/MFnPlugin.h>


const MString
DL_packageSpecific::getPackageDescriptionString()
{
	return MString(_3DELIGHT_PACKAGE_NAME);
}

void
DL_packageSpecific::set3dfmRibOption()
{
}

MStatus
DL_packageSpecific::registerNSIArchiveTranslator(MFnPlugin& i_plugin)
{
	return
		i_plugin.registerFileTranslator(
			MString("3Delight NSI Archive"),
			"",
			DL_nsiArchiveTranslator::creator,
			"NAT_optionsScript",
			"",
			true,
			MFnPlugin::kDefaultDataLocation);
}

MStatus
DL_packageSpecific::deregisterNSIArchiveTranslator(MFnPlugin& i_plugin)
{
	return i_plugin.deregisterFileTranslator(MString("3Delight NSI Archive"));
}

