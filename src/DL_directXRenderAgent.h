/*
  Copyright (c) 2014 The 3Delight Team.
*/

#ifndef __DL_DIRECTXRENDERAGENT_H__
#define __DL_DIRECTXRENDERAGENT_H__

#include <maya/MTypes.h>

/* DirectX only. */
#ifdef _WIN32
#if MAYA_API_VERSION >= 201400

#include <maya/MMatrix.h>
#include <maya/MString.h>

// DX stuff
#include <d3d11.h>
#include <d3dx11.h>
#include <xnamath.h>

#include <vector>

/*
 * The class contains all the common code for DirectX viewport. This class can
 * save dx buffers of an object and then draw it with a vertex and pixel shader.
 * The shaders are also created and compiled with this class.
 *
 * The info about shader:
 * Constant buffer passes external parameters (the transform matrix and
 * the object color) to the pixel and vertex shaders. Vertex shader applies
 * the transform matrix to the object (to every vertex). Pixel shader applies
 * the color to the object.
 */
class DL_directXRenderAgent
{
public:
  static DL_directXRenderAgent* getAgent();

  /* Main DirectX device */
  ID3D11Device* getDevice();

  /* Init DX shaders and buffers */
  void init();

  /* Sets up the matrices and the color  */
  void beginDraw(
      const MMatrix& i_world_view_matrix,
      const MMatrix& i_projection_matrix,
      const float* i_color);

  /* Draws saved object */
  void draw(int i_object);

  /* Saves object and returns ID of saved object */
  int addObject(
      ID3D11Buffer* i_vertex_buffer,
      ID3D11Buffer* i_index_buffer,
      int i_indexes );

  /* Releaze all the saved DX resources */
  void releaseResources();

private:
  DL_directXRenderAgent();

  /* Creates, compiles and stores pixel and vertex shaders. */
  bool initShaders();

  /* Creates a constant buffer that is used to pass external parameters to
   * the pixel and vertex shaders. */
  bool initBuffers();

  /* Draw a wireframe object using DX buffers */
  void drawSingleBuffer(
      ID3D11Buffer* i_vertex_buffer,
      ID3D11Buffer* i_index_buffer,
      int i_indexes );

  /* Returns a simple pixel and vertex shader */
  const MString& getShaderCode();

  struct D3D_Object
  {
    D3D_Object(
        ID3D11Buffer* i_vertex_buffer,
        ID3D11Buffer* i_index_buffer,
        int i_indexes ) :
      m_d3d_vertex_buffer(i_vertex_buffer),
      m_d3d_index_buffer(i_index_buffer),
      m_d3d_plane_num_indexes(i_indexes)
    {}

    ID3D11Buffer* m_d3d_vertex_buffer;
    ID3D11Buffer* m_d3d_index_buffer;
    int m_d3d_plane_num_indexes;
  };

  struct ConstantBufferDef
  {
    XMMATRIX fWVP;
    XMFLOAT4 fMatColor;
  };

  /* Direct3D devices */
  ID3D11Device* m_d3d_device;
  ID3D11DeviceContext* m_d3d_device_context;

  /* List of objects */
  std::vector<D3D_Object> m_d3d_buffers;

  /* Pixel and vertex shaders */
  ID3D11Buffer* m_d3d_constant_buffer;
  ID3D11VertexShader* m_d3d_vertex_shader;
  ID3D11PixelShader* m_d3d_pixel_shader;
  ID3D11InputLayout* m_d3d_vertex_layout;

  MString m_shader_location;
};

#endif // MAYA_API_VERSION
#endif //_WIN32

#endif
