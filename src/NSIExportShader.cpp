#include "NSIExportShader.h"

#include <maya/MDagPath.h>
#include <maya/MFnAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <stack>

#include "OSLUtils.h"
#include "ShaderDataCache.h"
#include "DL_utils.h"
#include "MU_typeIds.h"


static const char* sgk_bump_normal_source = "outColor";
static const char* sgk_bump_normal_target = "bumpNormal";

NSIExportShader::NSIExportShader(
	MObject i_object,
	const NSIExportDelegate::Context& i_context )
:
	NSIExportDelegate( i_object, i_context ),
	m_exist(false)
{
	MString newHandle = NSIShaderHandle( i_object, m_live );

	free( (void*)m_nsi_handle );
	m_nsi_handle = ::strdup( newHandle.asChar() );
}

NSIExportShader::~NSIExportShader()
{

}

void NSIExportShader::Create()
{
	NSI::Context nsi( m_nsi );
#ifdef DEBUG
	MFnDependencyNode depFn( Object() );
	MString name = depFn.name();
	// Simulate a comment in the output stream
	nsi.Evaluate( NSI::StringArg( "type", name.asChar() ) );
#endif
	nsi.Create( m_nsi_handle, "shader" );
}

void NSIExportShader::SetAttributes()
{
	MObject obj( Object() );
	MFnDependencyNode depFn( Object() );

	const ShaderData* shaderData = NSIExportUtilities::GetShaderData( obj );
	if( shaderData == 0x0 || shaderData->m_fullPathName.length() == 0 )
	{
		MString msg = MString( "3Delight for Maya: no OSL shader found for " ) +
			MString("node '" ) + depFn.name() + MString( "' of type '" ) +
			depFn.typeName() + MString( "'.");
			MGlobal::displayWarning( msg );

			return;
	}

	m_exist = true;

	NSI::Context nsi( m_nsi );

	NSI::ArgumentList argList;
	argList.Add(new NSI::CStringPArg(
		"shaderfilename", shaderData->m_fullPathName.asChar() ) );

	for( unsigned int i = 0; i < depFn.attributeCount(); i++ )
	{
		MObject attr = depFn.attribute( i );
		MPlug plug = depFn.findPlug( attr, true );

		SetOneShaderAttribute( plug, shaderData, argList );
	}

	if( argList.size() > 0 )
	{
		nsi.SetAttribute( m_nsi_handle, argList );
	}

}

void NSIExportShader::SetAttributesAtTime( double i_time, bool i_no_motion )
{
}

MObject NSIExportShader::GetConnectedShadingNode(
	const MPlug& i_plug,
	MString& o_otherPlugParamName )
{
	MObject obj;

	// Only deal with incoming connections from other shading nodes.
	MPlugArray incoming;
	i_plug.connectedTo( incoming, true, false );

	// Maya API states that there will be 0 or 1 element in incoming.
	if( incoming.length() >= 1 )
	{
		MObject connectedNode = incoming[0].node();
		int componentIndex = -1;

		const ParamData* paramData = GetParamDataForConnectedAttr(
			incoming[0],
			NSIExportUtilities::eOutput,
			0x0,
			componentIndex );

		if( paramData == 0x0 )
		{
			return obj;
		}

		o_otherPlugParamName = GetParamName( paramData, componentIndex );

		return connectedNode;
	}

	return obj;
}

const ParamData* NSIExportShader::GetParamDataForConnectedAttr(
	const MPlug& i_plug,
	NSIExportUtilities::IoType i_ioType,
	const ShaderData* i_shaderData,
	int& o_componentIndex )
{
	o_componentIndex = -1;

	if( i_shaderData == 0x0 )
	{
		i_shaderData = NSIExportUtilities::GetShaderData( i_plug.node() );
	}

	if( i_shaderData == 0x0 )
	{
		return 0x0;
	}

	// Locate the param data that corresponds directly to i_plug
	const ParamData* paramData =
		i_shaderData->GetParameterData( i_plug, i_ioType );

	if( paramData != 0x0 )
	{
		if( i_plug.isElement() )
		{
			o_componentIndex = i_plug.logicalIndex();
		}
		else if(NSIExportUtilities::IsChildOfArrayOfCompoundAttr( i_plug ) )
		{
			o_componentIndex = i_plug.parent().logicalIndex();
		}
		return paramData;
	}

	/*
		Attempt to handle child plugs as color, point, vector, uv or normal
		components. This requires a parent plug with 2 or 3 children.
	*/
	MPlug parent = i_plug.parent();
	if( !i_plug.isChild() ||
		parent.numChildren() > 3 || parent.numChildren() < 2 )
	{
		return 0x0;
	}

	// Locate the shader parameter related to the parent attribute
	paramData = i_shaderData->GetParameterData( parent, i_ioType );

	/*
		The shader param related to the parent attr must be some variant of a
		float[2] or float[3].
	*/
	if( !paramData )
		return 0x0;

	bool isFloat2 =
		paramData->m_type.elementtype == NSITypeFloat &&
		paramData->m_type.arraylen == 2;

	bool isFloat3 =
		paramData->m_type.IsOneColor() ||
		paramData->m_type.IsOnePoint() ||
		paramData->m_type.IsOneVector() ||
		paramData->m_type.IsOneNormal() ||
		(paramData->m_type.elementtype == NSITypeFloat &&
		 paramData->m_type.arraylen == 3);

	if( !isFloat2 && !isFloat3 )
		return 0x0;

	// Figure child index
	unsigned int i = -1;
	for( i = 0; i < parent.numChildren(); i++ )
	{
		if( parent.child( i ).attribute() == i_plug.attribute())
		{
			break;
		}
	}

	if( i < parent.numChildren() )
	{
		o_componentIndex = i;
		return paramData;
	}

	return 0x0;
}

MString NSIExportShader::GetParamName(
	const ParamData* i_paramData,
	int i_componentIndex )
{
	MString name( i_paramData->m_name.c_str() );
	if( i_componentIndex >= 0 )
	{
		name += MString( "[" ) + i_componentIndex + MString( "]" );
	}

	return name;
}

void NSIExportShader::Connect( const DelegateTable* )
{
	MObject obj( Object() );

	MFnDependencyNode depFn( Object() );

	// Make sure this node is a supported shader.
	const ShaderData* shaderData = NSIExportUtilities::GetShaderData( obj );
	if( shaderData == 0x0 )
	{
		return;
	}

	NSI::Context nsi( m_nsi );
	NSI::ArgumentList argList;

	// Get all connected plugs
	MPlugArray connectedPlugs;
	depFn.getConnections( connectedPlugs );

	for(unsigned int i = 0; i < connectedPlugs.length(); i++)
	{
		// Skip output attributes
		MFnAttribute attrFn( connectedPlugs[ i ].attribute() );
		if( !attrFn.isWritable() )
		{
			continue;
		}

		if( attrFn.object().hasFn( MFn::kMessageAttribute ) )
		{
			/*
				The connected node name / handle is passed as a string parameter by
				SetOneShaderAttribute.
			*/
			continue;
		}

		int componentIndex = -1;
		MString paramNameSuffix;

		const ParamData* paramData = GetParamDataForConnectedAttr(
			connectedPlugs[ i ],
			NSIExportUtilities::eInput,
			shaderData,
			componentIndex );

		if( paramData == 0x0 )
			continue;

		MString paramName = GetParamName( paramData, componentIndex );

		/*
			We only deal with incoming connections from other shading nodes.
		*/
		MString otherParamName;
		MObject otherObj =
			GetConnectedShadingNode( connectedPlugs[ i ], otherParamName );

		if( !otherObj.isNull() )
		{
			MString otherHandle = NSIShaderHandle( otherObj, m_live );

			nsi.Connect(
				otherHandle.asChar(), otherParamName.asChar(),
				m_nsi_handle, paramName.asChar() );

			if(IsBump2DValue(paramData->m_name.c_str()))
			{
				nsi.Connect(
					otherHandle.asChar(), sgk_bump_normal_source,
					m_nsi_handle, sgk_bump_normal_target );
			}
		}
	}

	if( !argList.empty() )
	{
		nsi.SetAttribute( m_nsi_handle, argList );
	}
}

const ParamData* NSIExportShader::SetOneShaderAttribute(
	const MPlug& i_plug,
	const ShaderData* i_shaderData,
	NSI::ArgumentList& o_argList )
{
	// Skip invalud i_plug or i_plug designating output attributes.
	MFnAttribute attrFn( i_plug.attribute() );
	if( i_plug.isNull() || !attrFn.isWritable() )
	{
		return 0x0;
	}

	/*
		Skip attribute if it has incoming connection from another supported
		shading node parameter.

		We always set values for children of array of compound attr because
		it is possible that only some elements are connected; the other will
		need to get proper values.
	*/
	if( !NSIExportUtilities::IsChildOfArrayOfCompoundAttr( i_plug ) )
	{
		MString dummy;
		MObject otherNode = GetConnectedShadingNode( i_plug, dummy );
		if( !otherNode.isNull() && !i_plug.isArray())
		{
			return 0x0;
		}
	}

	const ParamData* paramData =
		MayaShaderAttributeToNSIAttribute(
			i_plug,
			NSIExportUtilities::eInput,
			i_shaderData,
			m_live,
			o_argList );

	if( paramData &&
		paramData->m_flags & ParamData::Flag_ConnectUVCoord )
	{
		/*
			Connect the uvChooser node to this parameter if the proper flag is
			set.
		*/
		NSI::Context nsi( m_nsi );
		if( i_plug.numConnectedChildren() == 0 )
		{
			/*
				No connected children. Connect uvCoord.o_outUV to this param directly.
			*/
			nsi.Connect(
				NSIUVCoordHandle().asChar(), "o_outUV",
				m_nsi_handle, paramData->m_name );
		}
		else if( i_plug.numConnectedChildren() == 1 )
		{
			/*
				Only one of the two children is connected. Connect each element of
				uvCoord.outUV to each child. One of these connections will be
				overridden later in the NSI output.
			*/
			std::string shaderParam0 = paramData->m_name + "[0]";
			std::string shaderParam1 = paramData->m_name + "[1]";

			nsi.Connect(
				NSIUVCoordHandle().asChar(), "o_outUV[0]",
				m_nsi_handle, shaderParam0 );

			nsi.Connect(
				NSIUVCoordHandle().asChar(), "o_outUV[1]",
				m_nsi_handle, shaderParam1 );
		}
	}

	return paramData;
}

void NSIExportShader::EditOneShaderAttribute( const MPlug& i_plug )
{
	const ShaderData* shaderData =
		NSIExportUtilities::GetShaderData( i_plug.node() );

	if( shaderData == 0x0 )
	{
		return;
	}

	NSI::ArgumentList argList;

	if( i_plug.array().isArray() && i_plug.isCompound() &&
		!NSIExportUtilities::IsColorAttr(i_plug) )
	{
		/*
			Handle edits on an array element that is a compound attribute as
			edits on each compound child.

			This happens when removing a ramp element; in this case i_plug would be
			ramp1.colorEntryList[1] for instance. But this must be avoided with
			color array value edits.
		*/
		for( unsigned i = 0; i < i_plug.numChildren(); i++ )
		{
			SetOneShaderAttribute( i_plug.child( i ), shaderData, argList );
		}
	}
	else if( i_plug.isElement() )
	{
		/*
			Handle edits on an array element as an edit on the array itself.
		*/
		SetOneShaderAttribute( i_plug.array(), shaderData, argList );
	}
	else
	{
		const ParamData* paramData =
			SetOneShaderAttribute( i_plug, shaderData, argList );

		if( paramData == 0x0  && i_plug.isChild() )
		{
			/*
				Attempt to handle edits on elements of compound attributes as
				edits on their parent. I.e. editing 'ripplesX' while the shader
				expects 'point ripples' will fail above and we try to re-output
				'ripples'.
			*/
			MPlug parent = i_plug.parent();
			SetOneShaderAttribute( parent, shaderData, argList );
		}
	}

	if( argList.size() > 0 )
	{
		NSI::Context nsi( m_nsi );
		nsi.SetAttribute( m_nsi_handle, argList );
		nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
	}
}

void NSIExportShader::EditOneShaderConnection(
	const MPlug& i_plug,
	const MPlug& i_otherPlug,
	bool i_connectionMade )
{
	// Only deal with connections on input attributes or message attributes.
	MFnAttribute attrFn( i_plug.attribute() );
	if( attrFn.object().hasFn( MFn::kMessageAttribute ) )
	{
		EditOneShaderAttribute( i_plug );
	}

	if( !attrFn.isWritable() )
		return;

	/*
		Figure the name of the shader parameters involved in this connection
		event.

		When one of the GetParamDataForConnectedAttr returns null, it's because
		- one of the involved nodes have no corresponding OSL shader,
		- one of the plugs have no correspoonding shader param
		- this is an outgoing connection involving i_plug, which will be handled
		  by the callback on i_otherPlug.node().
	*/

	int componentIndex = -1;

	const ParamData* paramData = GetParamDataForConnectedAttr(
			i_plug,
			NSIExportUtilities::eInput,
			0x0,
			componentIndex );

	if( paramData == 0x0 )
		return;

	NSI::Context nsi( m_nsi );

	MString paramName = GetParamName( paramData, componentIndex );

	paramData = GetParamDataForConnectedAttr(
		i_otherPlug,
		NSIExportUtilities::eOutput,
		0x0,
		componentIndex );

	if( paramData == 0x0 )
		return;

	MString otherParamName = GetParamName( paramData, componentIndex );

	MString otherHandle = NSIShaderHandle( i_otherPlug.node(), m_live );
	if( i_connectionMade )
	{
		nsi.Connect(
			otherHandle.asChar(), otherParamName.asChar(),
			m_nsi_handle, paramName.asChar() );

		if(IsBump2DValue(paramName))
		{
			nsi.Connect(
				otherHandle.asChar(), sgk_bump_normal_source,
				m_nsi_handle, sgk_bump_normal_target );
		}
	}
	else
	{
		nsi.Disconnect(
			otherHandle.asChar(), otherParamName.asChar(),
			m_nsi_handle, paramName.asChar() );

		if(IsBump2DValue(paramName))
		{
			nsi.Disconnect(
				otherHandle.asChar(), sgk_bump_normal_source,
				m_nsi_handle, sgk_bump_normal_target );
		}
	}

	nsi.RenderControl( NSI::CStringPArg( "action", "synchronize") );
}

bool NSIExportShader::RegisterCallbacks()
{
	MObject& obj = (MObject&)Object();

	MCallbackId id = MNodeMessage::addAttributeChangedCallback(
		obj, AttributeChangedCB, this );

	AddCallbackId( id );

	id = MNodeMessage::addNodeDirtyPlugCallback( obj, NodeDirtyCB, this );
	AddCallbackId( id );

	return true;
}

bool NSIExportShader::Exist() const
{
	return m_exist;
}

/**
	\brief Creates the single UV coordinate node used for this
	render.

*/
void NSIExportShader::CreateTextureUVCoordNode( NSIContext_t i_nsi )
{
	MString handle( NSITextureUVCoordHandle() );
	MString shaderFilename = OSLUtils::GetFullpathname( "uvCoord" );

	NSI::Context nsi( i_nsi );
	nsi.Create( handle.asChar(), "shader" );
	nsi.SetAttribute( handle.asChar(),
		NSI::CStringPArg( "shaderfilename", shaderFilename.asChar() ) );
}

MString NSIExportShader::NSITextureUVCoordHandle()
{
	return "*uvCoord";
}

MString NSIExportShader::NSIUVCoordHandle()
{
	MObject _this(Object()); /* to get rid of const */
	MObject node;
	if( IsPartOfEnvironmentShader(_this, node) )
	{
		return NSIHandle(node, m_live) + "*uvCoord";
	}

	return NSITextureUVCoordHandle();
}

bool
NSIExportShader::IsBump2DValue(const MString& i_paramName)const
{
	return
		i_paramName == "bumpValue" &&
		MFnDependencyNode(Object()).typeName() == "bump2d";
}

void NSIExportShader::AttributeChangedCB(
	MNodeMessage::AttributeMessage i_msg,
	MPlug& i_plug,
	MPlug& i_otherPlug,
	void* i_delegate)
{
	NSIExportShader* delegate = (NSIExportShader*)i_delegate;

	/*
		Note: Must be careful if we must handle kAttributeAdded events here.
		Dynamic compound attributes will trigger events for their child first,
		followed by the parent. Trying to interrogate the parent during the
		child event will cause a crash.

		Example: EditOneShaderAttribute for a colorR attribute will end up
		trying to output the parent attr (color). Trying to fetch the value of
		the color attr will crash for dynamic attributes.
	*/
	if( (i_msg & MNodeMessage::kAttributeSet) ||
		(i_msg & MNodeMessage::kAttributeArrayRemoved) ||
		(i_msg & MNodeMessage::kAttributeArrayAdded) )
	{
		delegate->EditOneShaderAttribute( i_plug );
	}
	else if( i_msg & MNodeMessage::kConnectionMade ||
		i_msg & MNodeMessage::kConnectionBroken )
	{
		bool made = i_msg & MNodeMessage::kConnectionMade;
		if( made )
		{
			/*
				This ugly piece of code checks if we need to connect a
				Environment Handle*uvCoord node to one of the attributes of
				this shader.  This can happen if one of the nodes upstream
				is a DL_ENVIRONMENTNODE type node.
			*/
			MString src_plug_name = NSIExportUtilities::AttributeName( i_plug );
			MObject environment;
			MObject node( i_otherPlug.node() );
			if( src_plug_name == "outColor" &&
				delegate->IsPartOfEnvironmentShader(node, environment) )
			{
				const ShaderData* shaderData =
					NSIExportUtilities::GetShaderData( delegate->Object() );

				MFnDependencyNode depFn( delegate->Object() );

				for( unsigned int i = 0; i < depFn.attributeCount(); i++ )
				{
					MObject attr = depFn.attribute( i );
					MPlug plug = depFn.findPlug( attr, true );

					const ParamData* paramData =
						shaderData->GetParameterData(
							plug, NSIExportUtilities::IoType::eInput );

					if( paramData &&
						(paramData->m_flags & ParamData::Flag_ConnectUVCoord) )
					{
						NSI::ArgumentList dummy_argList;

						delegate->SetOneShaderAttribute(
							plug, shaderData, dummy_argList );

						NSI::Context nsi( delegate->m_nsi );

						nsi.RenderControl(
							NSI::CStringPArg( "action", "synchronize") );
						return;
					}
				}
			}
		}

		delegate->EditOneShaderConnection( i_plug, i_otherPlug, made );
	}
}

/**
	\brief Called by Maya in some events, like while a float slider is dragged,
		or when a connected input node has changed.

	Originally from Dl_nodeWatchCmd.cpp
*/
void NSIExportShader::NodeDirtyCB(MObject&,	MPlug& i_plug, void* i_delegate)
{
	// Filter out some events we don't care about.

	MPlugArray sourcePlugs;
	i_plug.connectedTo(sourcePlugs, true, false);

	if( sourcePlugs.length() > 0 )
	{
		/*
			Do nothing if the dirty plug has an incoming connection. We are
			only interested in the plug that has actually been modified.
		*/
		return;
	}

	/*
		Since the node dirty events are interesting only for drag events of
		some specific gadgets (sliders & color pickers), we narrow down the
		number of events that are handled to numeric attributes and we add
		to that a special case of the "worldInverseMatrix" which seems not to
		update properly without this special case.
	*/
	MStatus status;
	MObject attr = i_plug.attribute();
	bool do_edit = false;

	if( attr.hasFn( MFn::kNumericAttribute ) ||
		attr.hasFn( MFn::kUnitAttribute ) )
	{
		do_edit = true;
	}
	else if( i_plug.node().hasFn( MFn::kPlace3dTexture ) )
	{
		MFnAttribute attrFn( attr );
		if( attrFn.name() == "worldInverseMatrix" )
		{
			do_edit = true;
		}
	}

	if( do_edit )
	{
		NSIExportShader* delegate = (NSIExportShader*)i_delegate;
		delegate->EditOneShaderAttribute( i_plug );
	}
}

/**
	\brief Returns true if a node is part of a shading network ending with
	a DL_ENVIRONMENTNODE.

	\param _this
		The node form which to start the search this the shading network head.
	\param o_node
		If this returns true, returns the MObject of type DL_ENVIRONMENTNODE.

	CAVEAT: In Live render, this node can be created before being connect to
	the actual shading network and this will return false as there is no way.
	Further action is needed (\ref AttributeChangedCB).
*/
bool NSIExportShader::IsPartOfEnvironmentShader( MObject &_this, MObject &o_node )
{
	// get rid of the freakin' const.
	MObject obj( _this );
	MStatus status;

	/*
		check head node as the code below could miss it if _this is the only
		node and its an environment.
	*/
	if( MFnDependencyNode(_this).typeId().id() ==
		MU_typeIds::DL_ENVIRONMENTNODE )
	{
		o_node = _this;
		return true;
	}

	/*
		This specifically looks for a connected dlEnvironmentShape node somewhere
		downstream in the shading network.

		Should we ever want to support other environment nodes (possibly defined by
		other plug-ins), we should widen the filter type and check for a
		classiification string, which would be more costly.

		Note: with the DAG iterator, using BreadthFirst was much slower, at least
		in older versions of Maya.
	*/
	MItDependencyGraph it(
		obj,
		MFn::kPluginShape,
		MItDependencyGraph::kDownstream,
		MItDependencyGraph::kDepthFirst,
		MItDependencyGraph::kNodeLevel,
		&status );

	for(; !it.isDone(); it.next() )
	{
		if( MFnDependencyNode( it.currentItem() ).typeId().id() ==
			MU_typeIds::DL_ENVIRONMENTNODE )
		{
			o_node = it.currentItem();
			return true;
		}
	}

	return false;
}
