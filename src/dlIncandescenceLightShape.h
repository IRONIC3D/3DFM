/*
  Copyright (c) 2017 The 3Delight Team.
*/

#ifndef __dlIncandescenceLightShape_h__
#define __dlIncandescenceLightShape_h__

#include <maya/MPxSurfaceShape.h>

class dlIncandescenceLightShape : public MPxSurfaceShape
{
  public:
    dlIncandescenceLightShape();
    virtual ~dlIncandescenceLightShape(); 

    virtual void postConstructor();
    virtual MStatus compute( const MPlug&, MDataBlock& );

    static void* creator();
    static MStatus initialize();

    static MTypeId id;

  private:

    // Input attributes
    static MObject s_color;
    static MObject s_intensity;
    static MObject s_exposure;
    static MObject s_geometry;
};

#endif
