#	ifndef ISPP_INVOKED
/*
	This file is run both through the C compiler and the Inno Setup
	Preprocessor.  The above conditional (and others like it) ensures ISPP only
	sees the parts if can understsand. This means only the 3 defines for the
	version number and the useless but unavoidable (because of nesting) header
	guard.
*/

/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers.                                   */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#endif

#ifndef __DL_VERSION_H
#define __DL_VERSION_H

#define _3DFM_MAJOR_VERSION 9
#define _3DFM_MINOR_VERSION 0
#define _3DFM_PATCH_RELEASE 1

#ifndef ISPP_INVOKED

#define DL_VSTRING2( major, minor, patch ) DL_VSTRING3( major, minor, patch )
#define DL_VSTRING3( major, minor, patch ) #major "." #minor "." #patch
#define _3DFM_VERSION_STRING \
	DL_VSTRING2( _3DFM_MAJOR_VERSION, _3DFM_MINOR_VERSION, _3DFM_PATCH_RELEASE )

// Returns only the 3dfm version number in a string
inline static
const char* DL_get3dfmVersionNumberString() { return _3DFM_VERSION_STRING; }

#endif

#endif

