#ifndef __DL_RENDERSTATE_H__
#define __DL_RENDERSTATE_H__

#include <stack>
#include <vector>

#include <maya/MFloatArray.h>
#include <maya/MString.h>
#include <maya/MObjectHandle.h>

class DL_renderState
{
public:
  enum e_iprState { IPR_STOPPED, IPR_STARTING, IPR_RUNNING };
  enum e_renderingState { RS_IDLE, RS_ABORTING, RS_RENDERING };

  static DL_renderState&  getRenderState();

  void                setCurrentFrame(float frame)
                      { m_currentFrame = frame; }

  MStatus             setRenderNode(const MString& render_node);

  void                setShutterOpen(double shutter_open)
                      { m_shutterOpen = shutter_open; }
  void                setShutterClose(double shutter_close)
                      { m_shutterClose = shutter_close; }

  void                setShadingRate(double shading_rate)
                      { m_shadingRate = shading_rate; }

  float               getCurrentFrame() const
                      { return m_currentFrame; }

  MString             getRenderNode() const;

  double              getShutterOpen() const
                      { return m_shutterOpen; }
  double              getShutterClose() const
                      { return m_shutterClose; }

  double              getShadingRate() const
                      { return m_shadingRate; }

  void setIprState( e_iprState i_ipr ) { m_ipr = i_ipr; }
  e_iprState getIprState() const { return m_ipr; }

  void setRenderingState( e_renderingState i_state);
  e_renderingState getRenderingState() const { return m_renderingState; }

  void setFaceId( bool i_faceid ) { m_faceid = i_faceid; }
  bool getFaceId( void ) const { return m_faceid; }


protected:

  DL_renderState();
  DL_renderState(const DL_renderState& other);
  DL_renderState& operator=(const DL_renderState& other);

  ~DL_renderState();

private:
  float             m_currentFrame;
  MObjectHandle     m_renderNode;

  double            m_shutterOpen;
  double            m_shutterClose;

  double            m_shadingRate;

  bool              m_faceid;

  e_iprState m_ipr;
  e_renderingState m_renderingState;
};

#endif
