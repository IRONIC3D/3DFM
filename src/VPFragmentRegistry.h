/*
	Copyright (c) The 3Delight Team.
	Copyright (c) soho vfx inc.
*/

#ifndef __VPFragmentRegistry__
#define __VPFragmentRegistry__

#include <maya/MFileObject.h>
#include <maya/MString.h>
#include <maya/MStringArray.h>

#include "DL_mstringSTLutils.h"

#include <map>
#include <mutex>

/**
	An object to keep track of the loaded viewport shading fragments.

	The purpose is to keep track of the fragments 3Delight for Maya adds to the
	fragment manager so they can be properly removed when the plug-in is
	unloaded, and to check for their file existence before adding them.
*/
class VPFragmentRegistry
{
public:
	~VPFragmentRegistry();

	/** \brief Returns the singleton instance, creating one if necessary */
	static VPFragmentRegistry* Instance();

	/** \brief Creates the singleton instance */
	static void CreateInstance();

	/**
		\brief Deletes the singleton instance. This will unload all registered
	shading fragments.
	*/
	static void DeleteInstance();

	/**	\brief Add a path where the fragment files may be found. */
	void AddPath( const MString& i_path );

	/** \brief Returns the current fragment search path. */
	MString SearchPath();

	/**
		\brief
		Find the loaded fragments for a specified node type name.

		\param i_nodeType
			The node type name to search the table for.

		\return A MStringArray pointer to the list of fragments in use by the
		specified node type. Null pointer is returned when no data was found for
		that node type name. The first array element is the name of the fragment
		that the shader override can announce to be using.
	*/
	const MStringArray* Find( const MString& i_nodeType );

	/**
		\brief
		Add the shader fragments for the specified node type, loading them if
		needed.

		\param i_nodeType
			The node type name.

		\param o_fragmentNames
			The names of the fragments loaded for that node type. The first element
			is the name of the fragment that the viewport shader override can
			announce to be using.
	*/
	void Add(	const MString& i_nodeType, MStringArray& o_fragmentNames );

	void Remove( const MString& i_nodeType );

	/**
		\brief
		Add shader fragments not associated with one node type.

		\param i_fragments
			The fragment names to add.

		\param i_fragmentGraphs
			The fragment graph names to add.
	*/
	void Add(
		const MStringArray& i_fragments,
		const MStringArray& i_fragmentGraphs );

	/**
		\brief
		Clear the cached data, and remove all fragments from Maya's fragment
		manager.
	*/
	void Clear();

	/**
		\brief Set the verbosity of the fragment registry operations
	*/
	void Verbosity( int i_value );

	/**
		\brief Get the verbosity of the fragment registry operations
	*/
	int Verbosity();

	/**
		\brief Utility function to add one unregistered shader fragment.

		This is a wrapper for VPUtilities::AddFragment that avoids calling the
		Maya fragment manager if the fragment xml file is not found, and that
		can report file search information when verbosity is set to 2. Added
		fragments are not registered and should be removed using
		RemoveFragment.

		\param i_fragmentName
			The name of the fragment to add.

		\param i_isGraph
			Wehter to add this as a fragment or a fragment graph.

		\param i_file
			The file object used for the fragment file lookup.
	*/
	bool AddFragment(
		const MString& i_fragmentName,
		bool i_isGraph,
		MFileObject& i_file );

	/**
		\brief Utility function to remove one fragment.

		This is a wrapper for VPUtilities::RemoveFragment that produces relevant
		information based on Verbosity().	The registry is not edited by this
		function.

		\param i_fragmentName
			The name of the fragment to remove.

		\returns true if the removal was successful.
	*/
	bool RemoveFragment( const MString& i_fragmentName );

private:
	VPFragmentRegistry();
	VPFragmentRegistry(VPFragmentRegistry&);

	void ReportInfo( const MString& i_msg, int i_threshold );

	static VPFragmentRegistry* m_instance;
	static std::mutex m_mutex;

	std::map<MString, MStringArray, delight_mstringLessThan> m_table;
	MString m_paths;
	int m_verbosity;
};

#endif
