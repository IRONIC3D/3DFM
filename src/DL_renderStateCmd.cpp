#include <maya/MGlobal.h>
#include <maya/MArgParser.h>
#include <maya/MSyntax.h>

#include "DL_renderState.h"

#include "DL_renderStateCmd.h"

extern "C" int DlCloudEnabledQuery();

MSyntax
DL_renderStateCmd::newSyntax()
{
  MSyntax syntax;

  syntax.addFlag("-f", "-frame", MSyntax::kDouble);
  syntax.addFlag("-qf", "-query_frame");

  syntax.addFlag("-rn","-render_node", MSyntax::kString);
  syntax.addFlag("-qrn","-query_render_node");

  syntax.addFlag("-so","-shutter_open", MSyntax::kDouble);
  syntax.addFlag("-qso","-query_shutter_open");

  syntax.addFlag("-sc","-shutter_close", MSyntax::kDouble);
  syntax.addFlag("-qsc","-query_shutter_close");

  syntax.addFlag("-sr","-shading_rate", MSyntax::kDouble);
  syntax.addFlag("-qsr","-query_shading_rate");

  syntax.addFlag("-irs", "-ipr_state", MSyntax::kUnsigned);

  syntax.addFlag("-rs", "-rendering_state", MSyntax::kUnsigned);

  syntax.addFlag("-fi", "-faceid", MSyntax::kBoolean );

  syntax.enableQuery(true);

  return syntax;
}

MStatus
DL_renderStateCmd::doIt(const MArgList& args)
{
  MStatus         status;
  MArgParser      argParser(syntax(), args, &status);

  DL_renderState& render_state = DL_renderState::getRenderState();

  if (status == MS::kSuccess)
  {
    if (argParser.isFlagSet("-qf")
      || (argParser.isQuery() && argParser.isFlagSet("-f")))
    {
      setResult(render_state.getCurrentFrame());
    }
    else if (argParser.isFlagSet("-f"))
    {
      double frame_num;

      argParser.getFlagArgument("-f", 0, frame_num);

      render_state.setCurrentFrame(frame_num);
    }
    else if (argParser.isFlagSet("-qrn")
      || (argParser.isQuery() && argParser.isFlagSet("-rn")))
    {
      setResult(render_state.getRenderNode());
    }
    else if (argParser.isFlagSet("-rn"))
    {
      MString render_node;

      argParser.getFlagArgument("-rn", 0, render_node);

      render_state.setRenderNode(render_node);
    }
    else if (argParser.isFlagSet("-qso")
      || (argParser.isQuery() && argParser.isFlagSet("-so")))
    {
      setResult(render_state.getShutterOpen());
    }
    else if (argParser.isFlagSet("-so"))
    {
      double shutter_open;

      argParser.getFlagArgument("-so", 0, shutter_open);

      render_state.setShutterOpen(shutter_open);
    }
    else if (argParser.isFlagSet("-qsc")
      || (argParser.isQuery() && argParser.isFlagSet("-sc")))
    {
      setResult(render_state.getShutterClose());
    }
    else if (argParser.isFlagSet("-sc"))
    {
      double shutter_close;

      argParser.getFlagArgument("-sc", 0, shutter_close);

      render_state.setShutterClose(shutter_close);
    }
    else if (argParser.isFlagSet("-qsr")
      || (argParser.isQuery() && argParser.isFlagSet("-sr")))
    {
      setResult(render_state.getShadingRate());
    }
    else if (argParser.isFlagSet("-sr"))
    {
      double shading_rate;

      argParser.getFlagArgument("-sr", 0, shading_rate);

      render_state.setShadingRate(shading_rate);
    }
    else if (argParser.isFlagSet("-irs"))
    {
      if (argParser.isQuery())
        setResult(render_state.getIprState());
      else
      {
        int ipr_state = 0;
        argParser.getFlagArgument("-irs", 0, ipr_state);
        render_state.setIprState(
          static_cast<DL_renderState::e_iprState>(ipr_state));
      }
    }
    else if (argParser.isFlagSet("-rs"))
    {
      if (argParser.isQuery())
        setResult(render_state.getRenderingState());
      else
      {
        int rendering_state = 0;
        argParser.getFlagArgument("-rs", 0, rendering_state);
        render_state.setRenderingState(
          static_cast<DL_renderState::e_renderingState>(rendering_state));
      }
    }
    else if(argParser.isFlagSet("-fi"))
    {
      bool faceid = false;
      argParser.getFlagArgument( "-fi", false, faceid );
      render_state.setFaceId(faceid);
    }
  }

  return status;
}
