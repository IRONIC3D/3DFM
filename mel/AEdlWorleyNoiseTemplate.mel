/*
   Copyright (c) 2012 soho vfx inc.
   Copyright (c) 2018 The 3Delight Team.
   */

global proc
AE_MAT_worleyLayerChanged(string $node)
{
  int $layer_count = getAttr($node + ".layers");
  int $is_singlelayer = 1;
  if ($layer_count > 1)
    $is_singlelayer = 0;

  editorTemplate -dimControl $node "layer_persistence" $is_singlelayer;
  editorTemplate -dimControl $node "layer_scale" $is_singlelayer;
  editorTemplate -dimControl $node "layer_density" $is_singlelayer;
}


global proc
AE_MAT_worleyDistanceTypeChanged(string $node)
{
  int $dist_type = getAttr($node + ".distance_type");
  int $minkowskyOff = 1;
  if ($dist_type == 3)
    $minkowskyOff = 0;

  editorTemplate -dimControl $node "minkowski_k" $minkowskyOff;
}


global proc
AE_worleyColorRampInCellNew(string $curve_plug)
{
  string $list_msg = "Color Inside of Cell";
  text -label $list_msg -align "center" -height 20 -font "smallObliqueLabelFont" -enable false;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;
}


global proc
AE_worleyColorRampAcrossCellNew(string $curve_plug)
{
  string $list_msg = "Color Across Cells";
  text -label $list_msg -align "center" -height 20 -font "smallObliqueLabelFont" -enable false;

  AEmakeRampControlInteractiveNew $curve_plug;
  setParent ..;
}


global proc
AE_worleyColorRampReplace(string $curve_plug)
{
  AEmakeRampControlInteractiveReplace $curve_plug;
}


global proc
AEdlWorleyNoiseTemplate(string $node)
{
  editorTemplate -beginScrollLayout;

  // Main Worley
  AE_addSeparatorWithLabel( "worleySep", "Worley Noise", 75 );
  editorTemplate -label "Output Type" -addDynamicControl "output_type";
  editorTemplate -label "Distance Type"
    -addDynamicControl "distance_type" "AE_MAT_worleyDistanceTypeChanged";
  editorTemplate -label "Minkowski Number" -addDynamicControl "minkowski_n";
  AE_addSpacer( 12 );

  editorTemplate -label "Scale" -addDynamicControl "scale";
  editorTemplate -label "Density" -addDynamicControl "density";
  editorTemplate -label "Random Position" -addDynamicControl "random_pos";

  AE_addSpacer( 12 );

  editorTemplate -label "Time" -addDynamicControl "time";
  editorTemplate -label "Space" -addDynamicControl "space";

  // Colors
  AE_addSeparatorWithLabel( "colorSep", "Colors", 50 );
  editorTemplate -callCustom
    "AE_worleyColorRampInCellNew"
    "AE_worleyColorRampReplace"
    "color_in_cell";
  editorTemplate -callCustom
    "AE_worleyColorRampAcrossCellNew"
    "AE_worleyColorRampReplace"
    "color_across_cell";
  editorTemplate -label "Blend Mode" -addDynamicControl "color_blend_mode";
  editorTemplate -label "Edge Color" -addDynamicControl "color_edge";
  editorTemplate -label "Background Color" -addDynamicControl "color_bg";

  // Distortion
  AE_addSeparatorWithLabel( "distortionSep", "Distortion", 75 );
  editorTemplate -label "Distortion" -addDynamicControl "distortion";
  editorTemplate -label "Intensity" -addDynamicControl "distortion_intensity";

  // Borders
  AE_addSeparatorWithLabel( "borderSep", "Borders", 58 );
  editorTemplate -label "Thickness" -addDynamicControl "border_thickness";
  editorTemplate -label "Smoothness" -addDynamicControl "border_smoothness";

  // Layers
  AE_addSeparatorWithLabel( "layerSep", "Layers", 58 );
  AE_addControlWithCallback( "layers", "AE_MAT_worleyLayerChanged" );
  editorTemplate -label "Persistence" -addDynamicControl "layer_persistence";
  editorTemplate -label "Scale" -addDynamicControl "layer_scale";
  editorTemplate -label "Density" -addDynamicControl "layer_density";

  // Adjust
  AE_addSeparatorWithLabel( "adjustSep", "Adjust", 58 );
  editorTemplate -label "Invert" -addDynamicControl "invert";
  editorTemplate -label "Amplitude" -addDynamicControl "amplitude";
  editorTemplate -label "Contrast" -addDynamicControl "contrast";

  editorTemplate -addExtraControls -extraControlsLabel "";
  editorTemplate -suppress placementMatrix;
  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress "uvCoord";
  editorTemplate -suppress "incandescence_multiplier";

  editorTemplate -suppress "usedBy3dfm";
  editorTemplate -suppress "version";
  editorTemplate -suppress "frozen";

  editorTemplate -endScrollLayout;
}
