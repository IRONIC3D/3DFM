global int $gMaxNumOptionMenuItems = 30;

global string $gOptionMenuChoiceDlgCommand;
global string $gOptionMenuChoiceDlgItems[];

global proc
DL_optionMenuChoiceDlgHandleDoubleClick()
{
  //
  // Description:
  //  This procedure is called when the user double-clicks on an item in the
  //  text scroll list of the dialog created by DL_optionMenuChoiceDlg(). 
  //  This procedure invokes the command that was specified to
  //  DL_optionMenuChoiceDlg() when it was created, using the selected item in
  //  the text scroll list as the last argument to the command.
  //

  global string $gOptionMenuChoiceDlgCommand;
  global string $gOptionMenuChoiceDlgItems[];

  string $old_parent = `setParent -query`;
  setParent option_menu_choice_dlg_window;

  int $indices[] = 
    `textScrollList -query -selectIndexedItem item_list`;

  string $item = $gOptionMenuChoiceDlgItems[$indices[0] - 1];

  string $command = $gOptionMenuChoiceDlgCommand;
  $command += " \"" + $item + "\"";

  catch(eval($command));

  setParent $old_parent;

  // Now that the user has made their choice and the command has been
  // executed, we can close the dialog.
  //
  evalDeferred("deleteUI option_menu_choice_dlg_window");
}

global proc
DL_optionMenuChoiceDlg(
  string $items[],
  string $title,
  string $command)
{
  //
  // Description:
  //  When there are too many menu items to fit in an option menu (because 
  //  they wouldn't all fit on the screen), some option menus in the 3Delight
  //  UI will instead put just a single menu item in the option menu, which
  //  opens a dialog when chosen. This procedure creates that dialog. The
  //  specified items are displayed in a text scroll list. The dialog window 
  //  displays the specified title. The user can select the item they want by 
  //  double clicking on it, at which point the specified command is invoked,
  //  with the user's chosen item used as the last argument. 
  //

  global string $gOptionMenuChoiceDlgItems[];
  $gOptionMenuChoiceDlgItems = $items;

  global string $gOptionMenuChoiceDlgCommand;
  $gOptionMenuChoiceDlgCommand = $command;

  if (`window -exists option_menu_choice_dlg_window`)
  {
    deleteUI option_menu_choice_dlg_window;
  }

  string $window = 
    `window 
      -title $title 
      option_menu_choice_dlg_window`;

  string $form_layout = 
    `formLayout`;

    string $double_click_text =  
      `text 
        -label "Double-click an item to make your choice."
        double_click_text`;

    string $item_list = 
      `textScrollList 
        -allowMultiSelection false
        -doubleClickCommand "DL_optionMenuChoiceDlgHandleDoubleClick"
        item_list`;

    for ($item in $items)
    {
      textScrollList
        -edit
        -append $item
        $item_list;
    }

    formLayout -edit
      -attachForm $double_click_text "left" 5
      -attachForm $double_click_text "top" 5
      -attachForm $double_click_text "right" 5
      -attachNone $double_click_text "bottom"

      -attachForm $item_list "left" 5
      -attachControl $item_list "top" 5 $double_click_text
      -attachForm $item_list "right" 5
      -attachForm $item_list "bottom" 5
      $form_layout;

  setParent ..; // from $column_layout

  showWindow $window;
}

