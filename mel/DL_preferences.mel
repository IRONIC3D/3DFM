/*
	Copyright (c) 2008 soho vfx inc.
	Copyright (c) 2008 The 3Delight Team.
*/

global proc string
DPF_getImageViewer()
{
  // Returns the defined image viewer or i-display as a default
  //
  string $viewer = `optionVar -q "_3delight_viewer_image"`;

  if ($viewer == "0")
  {
    $viewer = getenv("DELIGHT") + "/bin/i-display";

    /* Uncomment when our apps can handle standard osx opening
    if (`about -os` == "mac")
    {
      $viewer += ".app";
    }
    */
  }

  return $viewer;
}

global proc string
DPF_getInteractiveRenderView()
{
  string $view = `optionVar -q "_3delight_render_view"`;
  if ($view == "0")
  {
    $view = "idisplay";
  }

  return $view;
}

global proc int
DPF_hasMultiLayerFramebuffer()
{
  return DL_isMultiLayerFrameBuffer(DPF_getInteractiveRenderView());
}

global proc
DPF_setImageViewer(string $viewer_path)
{
  optionVar -sv "_3delight_viewer_image" $viewer_path;
}

global proc
DPF_setInteractiveRenderView(string $view)
{
  optionVar -sv "_3delight_render_view" $view;
}

global proc
DPF_setScanning(string $scanning)
{
  optionVar -sv "_3delight_scanning" $scanning;
}

global proc
DPF_setProgressive( int $progressive )
{
  optionVar -iv "_3delight_progressive_render" $progressive;
}


global proc string DPF_getScanning()
{
  if( `optionVar -exists "_3delight_scanning"` )
  {
    string $scanning = `optionVar -q "_3delight_scanning"`;
    return $scanning;
  }

  return "Circle";
}

global proc int DPF_getProgressive()
{
  if( `optionVar -exists "_3delight_progressive_render"` )
  {
    int $p = `optionVar -q "_3delight_progressive_render"`;
    return $p;
  }

  return 0;
}


global proc
DPF_appFileBrowser(string $type)
{
  // A bare-bones file browser that defaults to some app directory
  // and that lets the user picks an application on all platforms.
  //
  float $version = getApplicationVersionAsFloat();
  string $default_dir;
  string $os = `about -os`;
  string $file_browser_cmd = "fileDialog";

  if ($version >= 2011)
    $file_browser_cmd += "2 -ff \"All Files (*.*)\" ";
  else
    $file_browser_cmd += " -m 0 ";

  int $file_mode = 1;

  switch($os)
  {
    case "mac" :
      $default_dir = "/Applications";

      if ($version >= 2011)
        $file_mode = 3;
      else
        $file_browser_cmd += "-app ";
      break;

    case "nt":
    case "win64":
      $default_dir = getenv("ProgramFiles");
      break;
    case "linux":
      $default_dir = "/usr/bin";
      break;
    default:
  }


  string $app = "";

  if ($version >= 2011)
  {
    string $caption = "Select application to view " +
      substring($type, 1, size($type) - size("Viewer")) + " files";

    $file_browser_cmd +=
      "-fm " + $file_mode + " " +
      "-dir \"" + $default_dir + "\" " +
      "-okCaption \"Select\" " +
      "-caption \"" + $caption + "\" ";
    string $selections[] = eval($file_browser_cmd);
    $app = $selections[0];
  }
  else
  {
    workspace -dir $default_dir;
    $app = eval($file_browser_cmd);
  }

  if ($app != "")
  {
    eval("DPF_set" + $type + " \"" + $app + "\"");
  }

  DPF_update();
}

global proc
DPF_fileBrowser(string $file_type, int $mode, string $set_pref_cmd)
{
  string $workspace = `workspace -q -fn`;
  string $cb = "DPF_fileBrowserCB " + $set_pref_cmd;

  setWorkingDirectory $workspace $file_type $file_type;

  if (getApplicationVersionAsFloat() >= 2011)
  {
    string $selection[] = `fileDialog2
      -cap "Select"
      -fm 1
      -ff "NSI files (*.nsi *.*)"`;

    if ($selection[0] != "")
      DPF_fileBrowserCB($set_pref_cmd, $selection[0], "rib");
  }
  else
    fileBrowser($cb, "Select", $file_type, $mode);
}

global proc
DPF_fileBrowserCB(string $set_pref_cmd, string $file_name, string $file_type)
{
  string $curr_dir = `workspace -q -dir`;

  retainWorkingDirectory($curr_dir);
  eval($set_pref_cmd + " \"" + $file_name + "\"");

  DPF_update();
}

global proc
DPF_updateTextFieldPref(string $text_field_name, string $set_pref_proc)
{
  // Meant to be called for a change command in a text field, this proc
  // retrieves the current text in the text field and calls the pref setting
  // function with it.
  //
  string $app = `textField -q -text $text_field_name`;
  eval($set_pref_proc + " \"" + $app + "\"");

  DPF_update();
}

global proc
DPF_updateFloatFieldPref(string $float_field_name, string $set_pref_proc)
{
  // Meant to be called for a change command in a float field group, this proc
  // retrieves the current value in the field and calls the pref setting
  // function with it.
  //

  string $value = `floatFieldGrp -q -value1 $float_field_name`;
  eval($set_pref_proc + " \"" + $value + "\"");

  DPF_update();
}

global proc
DPF_updateIntFieldPref(string $int_field_name, string $set_pref_proc)
{
  // Meant to be called for a change command in a int field group, this proc
  // retrieves the current value in the field and calls the pref setting
  // function with it.
  //

  string $value = `intFieldGrp -q -value1 $int_field_name`;
  eval($set_pref_proc + " \"" + $value + "\"");

  DPF_update();
}


global proc
DPF_scanningChangedCB(string $optionMenu )
{
  string $scanning = `optionMenuGrp -q -value $optionMenu`;
  DPF_setScanning( $scanning );
}

global proc
DPF_optionMenuChangedCB(string $optionMenu)
{
  string $view_name = `optionMenuGrp -q -value $optionMenu`;
  if($view_name == "Maya Render View")
  {
    DPF_setInteractiveRenderView("maya_render_view");
  }
  else if ($view_name == "Custom Frame Buffer")
  {
    DPF_setInteractiveRenderView("framebuffer");
  }
  else
  {
    DPF_setInteractiveRenderView("idisplay");
  }
}

global proc
DPF_init()
{
}

global proc
DPF_create()
{
  // General gadgets of the preferences window
  //
  columnLayout -adj true -rs 6 DFP_mainLayout;

  setUITemplate -pushTemplate "attributeEditorTemplate";

  text -h 12 -label "";

  string $menu =
    `optionMenuGrp
      -label "Render View"
      "DPF_renderViewOptionMenu"`;
    menuItem -label "3Delight Display";
    menuItem -label "Maya Render View";
    menuItem -label "Custom Frame Buffer";
    setParent -menu ..;

  optionMenuGrp
    -edit
    -changeCommand ("DPF_optionMenuChangedCB " + $menu)
    "DPF_renderViewOptionMenu";

  setParent ..; // from rowLayout;

  setUITemplate -popTemplate;

  DL_separator("");

  string $layout;
  $layout = AEfileButtonControl(
    "Image Viewer",
    "imageViewerField",
    "imageViewerBrowseButton",
    "");
  rowLayout -e -adj 2 -cw 3 40 -cat 3 "right" 20 $layout;

  string $image_cc = "DPF_updateTextFieldPref(\"imageViewerField\", \"DPF_setImageViewer\")";

  textField -e -cc $image_cc "imageViewerField";

  button -e -c "DPF_appFileBrowser ImageViewer" "imageViewerBrowseButton";

  setParent ..;
}

global proc
DPF_update()
{
  string $menu_item = "Maya Render View";
  string $view_name = DPF_getInteractiveRenderView();
  if($view_name == "idisplay")
  {
    $menu_item = "3Delight Display";
  }
  else if ($view_name == "framebuffer")
  {
    $menu_item = "Custom Frame Buffer";
  }
  optionMenuGrp -edit -value $menu_item "DPF_renderViewOptionMenu";

  textField -edit -text `DPF_getImageViewer` "imageViewerField";
}

global proc
DL_preferencesWindow()
{
  // Pop up the preferences window with a decent default size or the previous
  // size of the window
  //
  string $window_name = "delightPreferencesWindow";

  if (`window -exists $window_name`)
  {
    deleteUI $window_name;
  }

  int $width = 525;
  int $height = 225;

  window -title "3Delight Preferences" -width $width -height $height $window_name;

  DPF_create();
  DPF_update();

  showWindow $window_name;
}

global proc DPF_removeDeprecatedPreferences()
{
  string $vars[] =
  {
    "_3delight_showprereleasefeatures",
    "_3delight_shadernode_hideattributesthreshold",
    "_3delight_shaderpreview_primitivetype",
    "_3delight_shaderpreview_primitiveribarchive",
    "_3delight_shaderpreview_primitivescale",
    "_3delight_shaderpreview_shadingrate",
    "_3delight_viewer_shadowmap",
    "_3delight_viewer_photonmap",
    "_3delight_ipr_render_view",
    "_3delight_viewer_rib",
    "_3delight_viewer_log",
    "_3delight_viewer_statistics"
  };

  for($curr_var in $vars)
  {
    if (`optionVar -exists $curr_var`)
      optionVar -remove $curr_var;
  }
}
