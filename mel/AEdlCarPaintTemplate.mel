/*
  Copyright (c) The 3Delight Team.
*/

global proc AE_CAR_flakeWeightChanged( string $node )
{
  int $dim = 0;
  string $plug = $node + ".flake_weight";
  string $connections[] = `listConnections -destination false $plug`;

  if( size( $connections ) == 0 && getAttr( $plug ) <= 0 )
  {
    $dim = 1;
  }

  editorTemplate -dimControl $node "flake_density" $dim;
  editorTemplate -dimControl $node "flake_color" $dim;
  editorTemplate -dimControl $node "flake_roughness" $dim;
  editorTemplate -dimControl $node "flake_scale" $dim;
  editorTemplate -dimControl $node "flake_layers" $dim;
  editorTemplate -dimControl $node "flake_randomness" $dim;
}

global proc AEdlCarPaintTemplate( string $node )
{
  editorTemplate -beginScrollLayout;

  editorTemplate -callCustom AEshaderTypeNew AEshaderTypeReplace "message";

  // Base
  AE_addSeparatorWithLabel( "baseSep", "Base", 35 );
  editorTemplate -addDynamicControl -label "Color" "color";
  editorTemplate -addDynamicControl -label "Roughness" "diffuse_roughness";
  editorTemplate -addDynamicControl -label "Opacity" "opacity";

  // Specular
  AE_addSeparatorWithLabel( "specSep", "Specular", 40 );
  editorTemplate -addDynamicControl -label "Color" "specular_color";
  editorTemplate -addDynamicControl -label "Specular Level" "specular_level";
  editorTemplate -addDynamicControl -label "Roughness 1" "specular_roughness_1";
  editorTemplate -addDynamicControl -label "Roughness 2" "specular_roughness_2";
  editorTemplate -addDynamicControl -label "Balance" "specular_balance";
  editorTemplate -addDynamicControl -label "Metallic" "metallic";

  // Flakes
  AE_addSeparatorWithLabel( "flakesSep", "Flakes", 34 );
  editorTemplate
    -addDynamicControl
    -label "Weight"
    "flake_weight" "AE_CAR_flakeWeightChanged";
  editorTemplate -addDynamicControl -label "Density" "flake_density";
  editorTemplate -addDynamicControl -label "Color" "flake_color";
  editorTemplate -addDynamicControl -label "Roughness" "flake_roughness";
  editorTemplate -addDynamicControl -label "Scale" "flake_scale";
  editorTemplate -addDynamicControl -label "Layers" "flake_layers";
  editorTemplate -addDynamicControl -label "Randomness" "flake_randomness";

  // Coating
  AE_addSeparatorWithLabel( "coatingSep", "Coating", 40 );
  editorTemplate
    -addDynamicControl
    -label "Thickness"
    "coating_thickness"
    "AE_adjustCoatingAttributesSensitivity";
  editorTemplate -addDynamicControl -label "Color" "coating_color";
  editorTemplate -addDynamicControl -label "Roughness" "coating_roughness";
  editorTemplate
    -addDynamicControl -label "Specular Level" "coating_specular_level";

  // Thin Film
  AE_addSeparatorWithLabel( "thinFilmSep", "Thin Film", 50 );
  editorTemplate -addDynamicControl -label "Thickness" "thin_film_thickness";
  editorTemplate -addDynamicControl -label "IOR" "thin_film_ior";

  AE_addDispNormalBumpGadgets( 1 );

  AE_addGeometryGroup();
  AE_addAOVGroup("aovGroup");

  AE_addSpacer( 12 );

  editorTemplate -addExtraControls  -extraControlsLabel "";

  editorTemplate -suppress "uvCoord";
  editorTemplate -suppress caching;
  editorTemplate -suppress nodeState;
  editorTemplate -suppress usedBy3dfm;
  editorTemplate -suppress version;
  editorTemplate -suppress frozen;

  editorTemplate -endScrollLayout;
}
