/*
	Copyright (c) 2006 soho vfx inc.
	Copyright (c) 2006 The 3Delight Team.
*/

source DL_globals.mel;

source DL_aeCommon.mel;
source DL_aeFileHelpers.mel;
source DL_aeFileSequence.mel;
source DL_aeOverrideControls.mel;
source DL_attributeHelpers.mel;
source DL_aovSelector.mel;
source DL_commandLine.mel;
source DL_commonUI.mel;
source DL_colorManagement.mel;
source DL_extensionAttributes.mel;
source DL_globalPreferences.mel;
source DL_hypershade.mel;
source DL_idisplayFeedback.mel;
source DL_layersTreeView.mel;
source DL_lightFilter.mel;
source DL_lightUtilities.mel;
source DL_NSIOutputUtils.mel;
source DL_objectSelector.mel;
source DL_optionMenuChoiceDlg.mel;
source DL_outputVariables.mel;

source DL_renderView.mel;
source DL_nsiArchiveTranslator.mel;
source DL_scrollList.mel;
source DL_shelf.mel;
source DL_shutdown.mel;
source DL_preferences.mel;
source DL_versionUtilities.mel;

source dlAOVGroup.mel;
source dlAtmosphere.mel;
source dlColorCorrection.mel;
source dlColorVariation.mel;
source dlDecayFilterShape.mel;
source dlGoboFilterShape.mel;
source dlOpenVDBShader.mel;
source dlOpenVDBShape.mel;
source dlPrimitiveAttribute.mel;
source dlRenderGlobals.mel;
source dlRenderSettings.mel;
source dlViewportRenderSettings.mel;

source NSIRender.mel;

python("import DL_aeUtilities");
python("import DL_feedback");

global int $g_DL_pluginVersionWarningWasDisplayed;

global int $g_DL_sceneOpenedScriptJob;
global string $g_DL_sceneName;
global int $g_DL_aeSelectionChangedScriptJob;

global int $g_DL_startupWasExecuted = 0;
global int $g_DL_startupBatchWasExecuted = 0;

proc string
getMenuName(string $node_type, string $partial_menu_name)
{
  string $menu_name = "DL_" + $partial_menu_name + "RenderSettingsMenu";

  return $menu_name;
}

proc string
getNodeMenuName(string $partial_menu_name)
{
  string $menu_name = "DL_" + $partial_menu_name + "Menu";

  return $menu_name;
}

global proc
delightCreateMenus()
{
  global string $gMainWindow;

  if (`menu -exists delightMainWindowMenu`)
    deleteUI delightMainWindowMenu;

  if ($gMainWindow != "")
  {
    setParent $gMainWindow;

    menu -label "3Delight" -tearOff true delightMainWindowMenu;

      menuItem -label "Render"
               -image "shelf_launchRender.png"
               -command "DSH_launchRender";

      menuItem -label "Live Render (IPR)"
               -image "shelf_launchIPR.png"
               -command "DSH_launchIPR";

      menuItem -divider true;

      /* -- Render with ... */
      string $cmd = "DL_buildRenderSettingsMenu(\"Render\", "
        + "\"DL_renderMenuItemCommand\", 1, \"dlRenderSettings\")";

      string $menu_name = getMenuName("dlRenderSettings", "Render");

      menuItem -label "Render With"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      /* -- Batch render with ... */
      $cmd = "DL_buildRenderSettingsMenu(\"BatchRender\", "
        + "\"DL_batchRenderMenuItemCommand\", 1, \"nsiRenderSettings\")";
      $menu_name = getMenuName( "nsiRenderSettings", "BatchRender" );
      menuItem -label "Batch Render With"
               -subMenu true -tearOff false -postMenuCommand $cmd $menu_name;
      setParent -menu ..; //from submenu

      $menu_name = getMenuName("dlRenderSettings", "Abort");
      $cmd = "DL_buildAbortMenu( \"" + $menu_name + "\" )";
      menuItem
        -label "Abort Render"
        -subMenu true
        -tearOff false
        -postMenuCommand $cmd
        $menu_name;
      setParent -menu ..; //from submenu

      $cmd = "DL_buildRenderSettingsMenu(\"Select\", \"select\", 1, " +
        "\"dlRenderSettings\")";

      $menu_name = getMenuName("dlRenderSettings", "Select");
      menuItem -label "Select Render Settings"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      menuItem -label "Create Render Settings"
               -image "shelf_renderSettings.png"
               -annotation "Create new Render Settings"
               -command "select `RS_create`";

      $cmd = "DL_buildRenderSettingsMenu(\"Duplicate\", "
        + "\"duplicate -inputConnections\", 0, \"dlRenderSettings\")";

      $menu_name = getMenuName("dlRenderSettings", "Duplicate");
      menuItem -label "Duplicate Render Settings"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      menuItem -divider true;

      // Materials
      $menu_name = getNodeMenuName("Materials");
      $cmd = "DL_buildMaterialsMenu( \"" + $menu_name + "\" )";

      menuItem -label "Materials"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      // Lights
      $menu_name = getNodeMenuName("Lights");
      $cmd = "DL_buildLightsMenu( \"" + $menu_name + "\" )";

      menuItem -label "Lights"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      // Volumes
      $menu_name = getNodeMenuName("Volumes");
      $cmd = "DL_buildVolumesMenu( \"" + $menu_name + "\" )";

      menuItem -label "Volumes"
               -subMenu true
               -tearOff false
               -postMenuCommand $cmd
               $menu_name;
      setParent -menu ..; // from submenu

      // Using the same creation proc as the shelf button.
      menuItem -label "Create Set"
               -annotation "Create new Set"
               -command "DSH_createSet";

      menuItem -label "Create Stand-In"
               -annotation "Create new Stand-In"
               -command "createNode \"dlStandInNode\"";

      menuItem -divider true;

      menuItem -label "Preferences..."
               -annotation "Open 3Delight for Maya Preferences"
               -command "DL_preferencesWindow()";

      string $help_cmd =
        "\"http://3delightcloud.com/documentation/display/3DFM9/Introduction\"";

      string $os = `about -os`;
      if (match("linux", $os) != "")
        $help_cmd = "delightHelpLinux " + $help_cmd;
      else
        $help_cmd = "launch -web " + $help_cmd;

      menuItem -label "Help..."
               -annotation "View 3delight for Maya help"
               -command $help_cmd;

      menuItem -label "About..." -command "DL_aboutWindow";
  }
}

global proc
delightHelpLinux(string $url)
{
  string $browser = "firefox";
  if (system("which " + $browser) == "")
  {
    $browser = "chrome";
    if (system("which " + $browser) == "")
    {
      error("3Delight for Maya: can't find web broswer.\n" +
        "Help is available here: " + $url);
    }
  }

  exec($browser + " " + $url);
}

global proc
delightInstantiateScriptedPanels()
{
}

global proc
delightAddShelf()
{
  global string $gMainWindow;

  if ($gMainWindow != "")
    DSH_addShelf();
}

/* Returns full path to the MEL script that contains the input proc */
global proc string
DL_whereIsTheProc( string $procedure )
{
  string $where = "";

  if ( `exists $procedure` )
  {
    /* Use the whatIs command to determine the location. */
    string $result = eval( "whatIs " + $procedure );

    /* Parse the human-readable form. */
    string $tokens[];
    int $numTokens = `tokenize $result " " $tokens`;

    /* Make sure this is a MEL script and not an internal command. */
    if ( $tokens[0] == "Mel" )
    {
      /* Concatenate path if it contains spaces. */
      for ( $i = 4; $i < $numTokens; $i++ )
      {
        $where = $where + $tokens[$i];

        if ( $i < $numTokens )
        {
          $where = $where + " ";
        }
      }
    }
  }

  return $where;
}

proc
addDisplayString(string $key, string $value)
{
  if (!`displayString -exists $key`)
  {
    displayString -value $value $key;
  }
}

proc
removeDisplayString(string $key)
{
  if (`displayString -exists $key`)
  {
    displayString -delete $key;
  }
}

proc
getDisplayStringsKeyValuePairs(string $keys[], string $values[])
{
  $keys =
  {
    // Node type nice names
    //
    // Define the "niceName" entry that the nodeTypeNiceName procedure will look
    // for. This should be reflected in the Hypershade in general.
    //
    // Please keep these lists in alphabetical order of $keys values.
    //
    "n_dl3DelightMaterial.niceName",
    "n_dlAOVColor.niceName",
    "n_dlAOVFloat.niceName",
    "n_dlAOVGroup.niceName",
    "n_dlAOVPrimitiveAttribute.niceName",
    "n_dlAtmosphere.niceName",
    "n_dlColorBlend.niceName",
    "n_dlColorVariation.niceName",
    "n_dlColorCorrection.niceName",
    "n_dlDecayFilterShape.niceName",
    "n_dlGlass.niceName",
    "n_dlGoboFilterShape.niceName",
    "n_dlHairAndFur.niceName",
    "n_dlLayeredMaterial.niceName",
    "n_dlMetal.niceName",
    "n_dlOpenVDBShader.niceName",
    "n_dlPrimitiveAttribute.niceName",
    "n_dlSkin.niceName",
    "n_dlSubstance.niceName",
    "n_dlSky.niceName",
    "n_dlEnvironmentShape.niceName",
    "n_dlIncandescenceLightShape.niceName"
  };

  $values =
  {
    // Node type nice names
    "Legacy 3Delight Material",
    "Color AOV",
    "Float AOV",
    "AOV Group",
    "Primitive Attribute AOV",
    "Atmosphere",
    "Color Blend",
    "Color Variation",
    "Color Correction",
    "Decay Filter",
    "Glass",
    "Gobo Filter",
    "Hair & Fur",
    "Layered Material",
    "Metal",
    "Open VDB",
    "Primitive Attribute",
    "Skin",
    "Substance",
    "Sky",
    "Environment",
    "Incandescence Light"
  };

  if(size($keys) != size($values))
  {
    error("Malformed key - value arrays.");
  }
}

//
// Called by the C++ plug-in initialization function
//
global proc
DL_setupDisplayStrings()
{
  string $keys[];
  string $values[];
  getDisplayStringsKeyValuePairs($keys, $values);

  int $i;
  for($i = 0; $i < size($keys); $i++)
  {
    addDisplayString($keys[$i], $values[$i]);
  }
}

global proc
DL_removeDisplayStrings()
{
  string $keys[];
  string $values[];
  getDisplayStringsKeyValuePairs($keys, $values);

  int $i;
  for($i = 0; $i < size($keys); $i++)
  {
    removeDisplayString($keys[$i]);
  }
}

/*
  Returns the maya version specific custom template path based on the plug-in
  location.
*/
proc string
getCustomTemplatePath()
{
  string $plugins[] = `pluginInfo -q  -ls`;
  string $plugin_paths[]  = `pluginInfo -q -lsp`;

  // Not supposed to happen
  if( size( $plugins ) != size( $plugin_paths ) )
    return "";

  string $path = "";

  int $i;
  for( $i = 0; $i < size( $plugin_paths ); $i++ )
  {
    if( $plugins[$i] == "plugin_3delight_for_maya" )
    {
      $path = $plugin_paths[$i];
      break;
    }
  }

  if( $path == "" )
    return $path;

  // remove plugin/plugin_3delight_for_maya.bundle from $path
  $path = dirname( $path );
  $path = dirname( $path );
  $path += "/templates";

  return $path;
}

/*
  Adds a maya version-specific custom template path to the relevant env var.
*/
global proc
DL_addTemplatePath()
{
  // Add Maya version specific template path
  string $varname = "MAYA_CUSTOM_TEMPLATE_PATH";
  string $template_path = `getenv $varname`;
  string $path = getCustomTemplatePath();

  // Not supposed to happen
  if( $path == "" )
    return;

  if( `about -win` )
  {
    $path = ";" + $path;
  }
  else
  {
    $path = ":" + $path;
  }

  $path = $template_path + $path;

  putenv $varname $path;
}

/*
  Removes the path added by DL_addTemplatePath from the custom template path
  env var.
*/
global proc
DL_removeTemplatePath()
{
  string $varname = "MAYA_CUSTOM_TEMPLATE_PATH";
  string $template_path = `getenv $varname`;
  string $path = getCustomTemplatePath();

  string $sep = ":";
  if( `about -win` )
  {
    $sep = ";";
  }

  string $new_value = "";
  string $tokens[];
  tokenize( $template_path, $sep, $tokens );

  for( $curr_token in $tokens )
  {
    if( $curr_token == $path )
      continue;

    if( $new_value != "" )
    {
      $new_value += $sep;
    }

    $new_value += $curr_token;
  }

  putenv $varname $new_value;
}

//
// Plug-in initialization procedure when Maya is launched in batch mode. This
// can be either by using the "Render" executable, or starting maya with the
// -batch or -prompt falgs (the later is an interactive, window-less maya).
//
// NO UI RELATED CALLS IN HERE
// They fail and break the initialization.
//
global proc
DL_startupBatch()
{
  global int $g_DL_startupBatchWasExecuted;

  global int $g_DL_sceneOpenedScriptJob;

  $g_DL_sceneOpenedScriptJob =
    `scriptJob -cu true -e SceneOpened delightSceneOpenedCallback`;

  delightUpdateOldNodes();

  RG_startup();

  DPF_init();

  global string $g_no_timing_comments;
  $g_no_timing_comments = getenv("_3DFM_NO_TIMING_COMMENTS");

  // Declare some named conditions that can be watched for state == true
  //
  condition -state false dlRenderingStarted;
  condition -state false dlRenderingEnded;

  $g_DL_startupBatchWasExecuted++;
}

//
// Plug-in initialization procdure when Maya is launched with a graphical user
// interface.
//
// ONLY UI-RELATED CALLS HERE
// This procedure is NOT executed when running in batch mode
//
global proc
DL_startupUI()
{
  global int $g_DL_startupWasExecuted;

  if ($g_DL_startupWasExecuted)
  {
    return;
  }

  $g_DL_startupWasExecuted = 1;

  // Begin with the batch startup procedure
  DL_startupBatch();

  DL_addTemplatePath();

  DEA_init();

  delightAddShelf();

  delightCreateMenus();
  delightInstantiateScriptedPanels();

  DPF_removeDeprecatedPreferences();

  // Adding our render accelerator
  if (DL_haveMayaUI())
  {
    string $run_time_cmd = "_3DelightRender";
    string $name_cmd = "_3DelightRenderNameCommand";

    if (!`exists $run_time_cmd`)
    {
      // For proper listing in all HotKey Editor panels, we need:
      // a runTimeCommand that does something;
      // a nameCommand that calls a runTimeCommand;
      // a hotkey that is mapped to a nameCommand.
      //
      // We call DSH_launchRender to replicate the functionality of the shelf
      // render button, including its handling of the lack of a current NSI
      // render settings node.
      //
      runTimeCommand
        -default true
        -annotation "Render the active render settings with 3Delight."
        -category "3Delight"
        -command "DSH_launchRender()"
        $run_time_cmd;

      nameCommand
        -default true
        -annotation "Render the active render settings with 3Delight"
        -command $run_time_cmd
        $name_cmd;
    }


    // Only add our accelerator if there is none defined.
    // Oddly, we must search for the run-time cmd but define the hotkey for
    // the name cmd...
    //
    int $num_cmds = `assignCommand -q -numElements`;
    int $i;
    for ($i = 1; $i <= $num_cmds; $i++)
    {
      if (`assignCommand -q -command $i` == $run_time_cmd &&
        size(`assignCommand -q -keyArray $i`) == 0)
      {
        hotkey -k ";" -name $name_cmd;
        break;
      }
    }
  }

  // Trigger an attribute editor template refresh so that extension attributes
  // added on shapes that have already been displayed in the AE will be shown.
  //
  refreshEditorTemplates;

  // As a convenience, register filePathEditor attributes.
  //
  filePathEditor -registerType "dlOpenVDBShape.filename" -typeLabel "DlOpenVDBShape" -temporary;
}

//
// Deprecated
//
global proc
DL_startup()
{
  DL_startupUI();
}

global proc
DL_renderMenuItemCommand(string $node)
{
  undoInfo -stateWithoutFlush false;
  catch( NSI_interactiveRender( $node, false ) );
  undoInfo -stateWithoutFlush true;
}

global proc
DL_batchRenderMenuItemCommand(string $node)
{
  undoInfo -stateWithoutFlush false;
  catch( NSI_batchRender( $node ) );
  undoInfo -stateWithoutFlush true;
}


global proc
DL_buildRenderSettingsMenu(
  string $partial_menu_name,
  string $command,
  int $add_active_settings_item,
  string $node_type)
{
  string $menu_name = getMenuName($node_type, $partial_menu_name);

  // Delete all existing items from the menu, since we are going to rebuild it
  // completely.
  //
  menu -edit -deleteAllItems $menu_name;

  // Get a list of all render pass nodes that exist in the scene.
  //
  string $all_settings[] = RS_getAllRenderSettings();

  if (size($all_settings) > 0)
  {
    string $active_settings = "";
    if ($add_active_settings_item)
    {
      $active_settings = RG_getActiveRenderSettings();
    }

    global int $gMaxNumOptionMenuItems;

    if (size($all_settings) < $gMaxNumOptionMenuItems)
    {
      // Add to the menu a menu item for each render pass in the scene.
      //
      for ($curr_settings in $all_settings)
      {
        string $cmd;
        $cmd = "menuItem " +
          "-label " + $curr_settings + " " +
          "-parent " + $menu_name + " " +
          "-annotation \"" + $partial_menu_name + " " +
            $curr_settings + "\" " +
          "-command \"" + $command + " " + $curr_settings + "\"";

          if ($active_settings == $curr_settings)
            $cmd += "-checkBox true ";

        eval($cmd);
      }
    }
    else
    {
      // There are too many items in the scene to put in one menu.
      // Instead, we offer the user the option of opening a dialog
      // from which they can choose one
      //
      menuItem
        -enableCommandRepeat false
        -label "Too many items. Choose from dialog..."
        -parent $menu_name
        -command
          ("DL_optionMenuChoiceDlg "
            + DL_stringArrayAsString($all_settings)
            + " \"" + $partial_menu_name + "\""
            + " \"" + $command + "\"");
    }
  }
  else
  {
    // No render passes exist in the scene. Add only one item to the menu,
    // which tells the user there are no render passes.
    //
    menuItem
      -label "<no render settings>"
      -enable false
      -annotation "No 3Delight render settings in scene"
      -parent $menu_name;
  }
}

global proc
DL_buildAbortMenu( string $menu_name )
{
  menu -edit -deleteAllItems $menu_name;

  string $all_settings[] = RS_getAllRenderSettings();

  string $label;
  int $in_progress = 0;

  for($curr_setting in $all_settings)
  {
    $label = "";
    int $op = RS_getExportOperation($curr_setting);

    if( $op == 1 )
    {
      $label = $curr_setting;
    }
    else if ($op == 2 )
    {
      $label = $curr_setting + " (live)";
    }

    if($label != "")
    {
      $in_progress = 1;

      menuItem
        -label $label
        -command ("NSI_abort \"" + $curr_setting + "\"")
        -parent $menu_name;
    }
  }

  if( $in_progress == 0 )
  {
    menuItem
      -label "No Rendering in Progress"
      -enable false
      -parent $menu_name;
  }
}

global proc
DL_buildMaterialsMenu( string $menu_name )
{
  menu -edit -deleteAllItems $menu_name;

  string $classification = "rendernode/dl/shader/surface";
  string $materials[] = `listNodeTypes $classification`;

  for ($material in $materials)
  {
    string $labelName = `nodeTypeNiceName $material`;
    string $icon = "render_" + $material + ".png";

    string $command = "DSH_createAndAssignShader \"" + $material + "\"";

    menuItem
      -label $labelName
      -command $command
      -image $icon
      -parent $menu_name;
  }
}

global proc
DL_buildLightsMenu( string $menu_name )
{
  menu -edit -deleteAllItems $menu_name;

  menuItem -label "Directional Light"
           -annotation "Create new Directional Light"
           -command "DSH_createDirectionalLight"
           -image "shelf_dlDirectionalLight.png"
           -parent $menu_name;

  menuItem -label "Point Light"
           -annotation "Create new Point Light"
           -command "DSH_createQuadraticPointLight"
           -image "shelf_quadraticPointLight.png"
           -parent $menu_name;

  menuItem -label "Spot Light"
           -annotation "Create new Spot Light"
           -command "DSH_createQuadraticSpotLight"
           -image "shelf_quadraticSpotLight.png"
           -parent $menu_name;

  // Area Light
  string $area_light_menu_name = getNodeMenuName("AreaLights");
  string $area_light_cmd = "DL_buildAreaLightsMenu( \""
                            + $area_light_menu_name + "\" )";

  menuItem -label "Area Light"
           -subMenu true
           -tearOff false
           -postMenuCommand $area_light_cmd
           -image "shelf_quadraticAreaLight.png"
           -parent $menu_name
           $area_light_menu_name;
  setParent -menu ..; // from submenu

  menuItem -label "Environment Light"
           -annotation "Create new 3Delight Environment Light"
           -command "DSH_createEnvironmentLight"
           -image "shelf_dlEnvironmentShape.png"
           -parent $menu_name;

  menuItem -label "Incandescence Light"
           -annotation "Create new 3Delight Incandescence Light"
           -command "DSH_createIncandescenceLight"
           -image "shelf_dlIncandescenceLightShape.png"
           -parent $menu_name;
}

global proc
DL_buildAreaLightsMenu( string $menu_name )
{
  menu -edit -deleteAllItems $menu_name;

  menuItem -label "Square"
           -annotation "Create new Square Area Light"
           -command "DL_createQuadraticShapedAreaLight 0"
           -parent $menu_name;

  menuItem -label "Disk"
           -annotation "Create new Disk Area Light"
           -command "DL_createQuadraticShapedAreaLight 1"
           -parent $menu_name;

  menuItem -label "Sphere"
           -annotation "Create new Sphere Area Light"
           -command "DL_createQuadraticShapedAreaLight 2"
           -parent $menu_name;

  menuItem -label "Cylinder"
           -annotation "Create new Cylinder Area Light"
           -command "DL_createQuadraticShapedAreaLight 3"
           -parent $menu_name;
}

global proc
DL_buildVolumesMenu( string $menu_name )
{
  menu -edit -deleteAllItems $menu_name;

  menuItem -label "Open VDB"
           -annotation "Create new OpenVDB"
           -command "DL_createNode(\"dlOpenVDBShape\", \"\")"
           -image "shelf_dlOpenVDBShape.png"
           -parent $menu_name;

  menuItem -label "Atmosphere"
           -annotation "Create new Atmosphere"
           -command "DL_createNode(\"dlAtmosphere\", \"\")"
           -parent $menu_name;
}

global proc
delightUpdateAbortRenderMenuItem()
{
  string $abort_item = "delightAbortRenderMenuItem";
  if (`menuItem -q -exists $abort_item` == 0)
    return;

  int $render_state = `delightRenderState -q -rs`;
  if ($render_state == 1)
  {
    menuItem -edit -label "Aborting render..." -enable false $abort_item;
  }
  else
  {
    menuItem
        -edit
        -label "Abort Render"
        -command "delightBgRender -abort"
        -enable ($render_state == 2)
        $abort_item;
  }
}

global proc
DL_aboutWindow()
{
  string $about_window = "delightAboutWindow";

  if (!`window -q -exists $about_window`)
  {
    string $title = "About 3Delight";
    if (`about -os` == "mac")
    {
      // "About" dialogs on OS X do not have a window title. On other platforms,
      // an empty title string gets replaced with the name of the executable.
      // Booh.
      //
      $title = "";
    }

    window -w 300 -h 110 -title $title $about_window;

    string $layout = `columnLayout -adjustableColumn true`;

    text -h 25 -label "";
    text -label "<h3>3Delight for Maya</h3>";

    text -height 12 -label "";
    string $version = `delightAbout -rvs`;
    text -label ("Version <b>" + $version + "</b>");

    text -label "" -h 3;
    text -label ("Built on " + `delightAbout -buildDate`);

    if (`delightAbout -isFree`)
    {
      text -height 15 -label "";
      $label = "Using <b>Free</b> 3Delight License";
      text -label $label;
    }
    else
    {
      text -height 35 -label "";
    }

    text -label "" -h 15;

    text -label `delightAbout -copyrightString`;
    text -label "All rights reserved.";
    setParent ..;
  }

  showWindow $about_window;
}

// Callback mel procedures
//
global proc
delightAfterImportCallback()
{
  delightUpdateOldNodes();
}

global proc
delightSceneOpenedCallback()
{
  // This procedure is called by a script job on the "SceneOpened" event. This
  // appears to be triggered by a File -> Open Scene, and by File -> New Scene.
  //
  // Makes sure an active pass exists.
  RG_getActiveRenderSettings();
}
